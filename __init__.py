import sys
import tempfile
import copy
import shutil
import bpy
import traceback
from .sm64_import import *

# info about add on
bl_info = {
	"name": "SM64 Importer",
	"category": "Object",
	}

axis_enums = [	
	('X', 'X', 'X'), 
	('Y', 'Y', 'Y'), 
	('-X', '-X', '-X'),
	('-Y', '-Y', '-Y'),
]

# See SM64GeoLayoutPtrsByLevels.txt by VLTone
class SM64_ImportGeolayout(bpy.types.Operator):
	# set bl_ properties
	bl_idname = 'object.sm64_import_geolayout'
	bl_label = "Import Geolayout"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	geolayoutStart = bpy.props.IntProperty(name ='Geolayout Start (base 10)', default = 2039136)
	generateArmature = bpy.props.BoolProperty(name ='Generate Armature?')
	levelGeo = bpy.props.EnumProperty(items = level_enums, name = 'Level Used By Geolayout', default = 'HMC')
	importRom = bpy.props.StringProperty(name ='Import ROM', subtype = 'FILE_PATH')

	# Called on demand (i.e. button press, menu item)
	# Can also be called from operator search menu (Spacebar)
	def execute(self, context):
		try:
			romfileSrc = open(self.importRom, 'rb')
			readObjs = parseGeoLayout(romfileSrc, self.geolayoutStart, 
				self.levelGeo, None, blenderToSM64Rotation, useArmature = self.generateArmature)
			romfileSrc.close()

			bpy.ops.object.select_all(action = 'DESELECT')
			for readObj in readObjs:
				readObj.select = True

			bpy.ops.object.select_grouped(extend = True, type='CHILDREN_RECURSIVE')
			selectedObjs = bpy.context.selected_objects
			for selectedObj in selectedObjs:
				selectedObj['sm64_rom_start'] = 0
				selectedObj['sm64_rom_end'] = 0
				selectedObj['sm64_geolayout_pointers'] = []
				selectedObj['sm64_draw_layer'] = 0x01

			self.report({'INFO'}, 'Generic import succeeded.')
			return {'FINISHED'} # must return a set

		except:
			romfileSrc.close()
			self.report({'ERROR'}, traceback.format_exc())
			return {'CANCELLED'} # must return a set

class SM64_ImportDL(bpy.types.Operator):
	# set bl_ properties
	bl_idname = 'object.sm64_import_dl'
	bl_label = "Import Display List"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	displayListStart = bpy.props.IntProperty(name ='Import Display List Start (base 10)', default = 0xA3BE1C)
	levelDL = bpy.props.EnumProperty(items = level_enums, name = 'Level Used By Display List', default = 'CG')
	importRom = bpy.props.StringProperty(name ='Import ROM', subtype = 'FILE_PATH')

	# Called on demand (i.e. button press, menu item)
	# Can also be called from operator search menu (Spacebar)
	def execute(self, context):
		try:
			romfileSrc = open(self.importRom, 'rb')
			levelParsed = parseLevelAtPointer(romfileSrc, level_pointers[self.levelDL])
			segmentData = levelParsed.segmentData
			readObj = F3DtoBlenderObject(romfileSrc, self.displayListStart, context.scene,
				newname = 'sm64_mesh', segmentData = segmentData)
			readObj['sm64_level'] = self.levelDL
			readObj['sm64_geolayout_pointers'] = []
			romfileSrc.close()

			bpy.ops.object.select_all(action = 'DESELECT')
			readObj.select = True
			bpy.context.scene.objects.active = readObj
			#bpy.ops.object.scale_clear()
			readObj.matrix_world = blenderToSM64Rotation.to_4x4().inverted()
			#print(readObj.matrix_world)

			# Don't apply on children if you want leaf nodes of armature
			# to point in local space.
			bpy.ops.object.select_grouped(extend = True, type='CHILDREN_RECURSIVE')
			bpy.ops.object.transform_apply(rotation = True)

			self.report({'INFO'}, 'Generic import succeeded.')
			return {'FINISHED'} # must return a set

		except:
			romfileSrc.close()
			self.report({'ERROR'}, traceback.format_exc())
			return {'CANCELLED'} # must return a set

class SM64_ExportDL(bpy.types.Operator):
	# set bl_ properties
	bl_idname = 'object.sm64_export_dl'
	bl_label = "Export Display List"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	exportRom = bpy.props.StringProperty(name ='Export ROM', subtype = 'FILE_PATH')
	outputRom = bpy.props.StringProperty(name ='Output ROM', subtype = 'FILE_PATH')
	levelDL = bpy.props.EnumProperty(items = level_enums, 
		name = 'Level Used By Display List', default = 'CG')


	# Called on demand (i.e. button press, menu item)
	# Can also be called from operator search menu (Spacebar)
	def execute(self, context):

		try:
			romfileExport = open(self.exportRom, 'rb')
			shutil.copy(self.exportRom, self.outputRom)
			romfileExport.close()
			romfileOutput = open(self.outputRom, 'rb+')

			segmentData = {}
			ExtendBank0x04(romfileOutput, segmentData)
			DisableLowPolyMario(romfileOutput)
	
			allObjs = context.selected_objects
			bpy.ops.object.select_all(action = 'DESELECT')
			for obj in allObjs:
				if 'sm64_rom_start' in obj and \
					'sm64_rom_end' in obj and \
					'sm64_geolayout_pointers' in obj:
	
					start = obj['sm64_rom_start']
					end = obj['sm64_rom_end']
					geo = obj['sm64_geolayout_pointers']

					if 'sm64_draw_layer' in obj:
						drawLayer = obj['sm64_draw_layer']
					else:
						drawLayer = 0x01
	
					success = importGeneric(romfileOutput, obj, context.scene, 'Y', start, 
						end, self.levelDL, geo, blenderToSM64Rotation,
						drawLayer, verbose = True)
					if not success:
						self.report({'ERROR'}, 'Model is too large.')
						romfileOutput.close()
						return {'CANCELLED'} # must return a set
	

					obj.select = True
	
				else:
					self.report({'ERROR'}, obj.name + \
						" does not have correct custom properties.")
	
	
			romfileOutput.close()
			self.report({'INFO'}, 'Export succeeded.')
			return {'FINISHED'} # must return a set

		except:
			romfileOutput.close()
			self.report({'ERROR'}, traceback.format_exc())
			return {'CANCELLED'} # must return a set

def savePartSizeListToFile(partSizeList, interval, filepath, useLogFile):
	print(str(useLogFile))
	if not useLogFile:
		return
	logFile = open(filepath, 'w')

	header = 'Interval Start: ' + \
		'{:<10X}'.format(interval[0]) + "\n"
	header += 'Interval End:   ' + \
		'{:<10X}'.format(interval[1]) + "\n\n"

	header += '{:<10}'.format('Start')
	header += '{:>10}'.format('Size')
	header += '   '
	header += '{:<30}'.format('Name(s)')
	header += '\n'
	header += '{:-<50}'.format('') + '\n'
	logFile.write(header)

	tooLarge = False
	for partSize in partSizeList:
		nextLine = '{:<10X}'.format(partSize['start'])
		if partSize['size'] == 0:
			nextLine += '{:>10}'.format('failed')
		else:
			nextLine += '{:>10X}'.format(partSize['size'])
		nextLine += '   '
		nextLine += '{:<30}'.format(",".join(partSize['name']))
		nextLine += '\n'

		logFile.write(nextLine)
		tooLarge |= (partSize['size'] == 0)

	total = '{:-<50}'.format('') + '\n'
	if not tooLarge:
		total += '{:<10X}'.format(partSizeList[-1]['start'] + partSizeList[-1]['size'])
		total += '{:>10X}'.format(sum([partSize['size'] for partSize in partSizeList]))
		total += '   '
		total += '{:<30}'.format("")
		total += '\n'
	else:
		total += 'Total combined size is too large.'
	logFile.write(total)

	logFile.close()


class SM64_ExportMario(bpy.types.Operator):
	# set bl_ properties
	bl_idname = 'object.sm64_export'
	bl_label = "Export Mario"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	exportRom = bpy.props.StringProperty(name ='Export ROM', subtype = 'FILE_PATH')
	outputRom = bpy.props.StringProperty(name ='Output ROM', subtype = 'FILE_PATH')
	useFaceAnimation = bpy.props.BoolProperty(name ='Use Face Animation?')
	overwriteWingPositions = bpy.props.BoolProperty(name ='Overwrite Wing Positions? (See attatched diagram)')
	useCustomExportRange = bpy.props.BoolProperty(name ='Use custom export range? (Bank 0x04 will not be extended)')
	customExportRange = bpy.props.IntVectorProperty(name="Custom export range", 
		default=(0x823B64, 0x858EDC), size = 2)
	useLogFile = bpy.props.BoolProperty(name ='Write log file?')
	logFilePath = bpy.props.StringProperty(name = 'Log File', subtype = 'FILE_PATH')

	def execute(self, context):
		try:
			romfileExport = open(self.exportRom, 'rb')
			shutil.copy(self.exportRom, self.outputRom)
			romfileExport.close()
			romfileOutput = open(self.outputRom, 'rb+')
	
			geoStartAddress = 0x12A784
			drawLayer = 0x01
			segmentData = parseCommonSegmentLoad(romfileOutput)

			exportRange = self.customExportRange if self.useCustomExportRange \
				else marioFullRomInterval
			print("Export range: " + hex(exportRange[0]) + " - " + hex(exportRange[1]))
	
			if not self.useCustomExportRange:
				ExtendBank0x04(romfileOutput, segmentData)
			DisableLowPolyMario(romfileOutput)
	
			# Find armature.
			armatureObj = None
			armature = None
			for obj in bpy.context.selected_objects:
				if isinstance(obj.data, bpy.types.Armature):
					armatureObj = obj
					armature = armatureObj.data
					break
			if armature is None:
				romfileOutput.close()
				self.report({'ERROR'}, 'Mario armature not selected.')
				return {'CANCELLED'} # must return a set
	
			# Update geolayout translation offsets

			if self.overwriteWingPositions:
				saveGeolayoutTransforms(romfileOutput, armatureObj, 
					geoStartAddress, basicMarioArmatureData, blenderToSM64Rotation) 
			else:
				saveGeolayoutTransforms(romfileOutput, armatureObj, 
					geoStartAddress, basicMarioArmatureData, blenderToSM64Rotation, 
					['normal wing 2a', 'normal wing 2b'])

			bodyParts = []
			for obj in bpy.context.scene.objects:
				if isinstance(obj.data, bpy.types.Mesh) and\
					len(obj.data.loops) > 0:
	
					for constraint in obj.constraints:
						if isinstance(constraint, bpy.types.ChildOfConstraint) and\
							constraint.target == armatureObj:
	
							bodyParts.append(obj)
	
			success = importMario(bodyParts, self, romfileOutput, context, 'Y', segmentData, armatureObj,
				exportRange[0], exportRange[1], geoStartAddress)

			armatureObj.select = True
			romfileOutput.close()
	
			if success:
				self.report({'INFO'}, 'Export succeeded.')
				return {'FINISHED'} # must return a set
			else:
				self.report({'INFO'}, 'Export failed.')
				return {'CANCELLED'} # must return a set
		except:
			self.report({'ERROR'}, traceback.format_exc())
			romfileOutput.close()
			return {'CANCELLED'} # must return a set

class SM64_ImportMario(bpy.types.Operator):
	# set bl_ properties
	bl_idname = 'object.sm64_import'
	bl_label = "Import Mario"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	importRom = bpy.props.StringProperty(name ='Import ROM', subtype = 'FILE_PATH')
	customHatless = bpy.props.BoolProperty(name ='Also import hatless head?')

	# Called on demand (i.e. button press, menu item)
	# Can also be called from operator search menu (Spacebar)
	def execute(self, context):
		try:
			romfileSrc = open(self.importRom, 'rb')
	
			# Mario
			readObjs = parseGeoLayout(romfileSrc, 0x12A784, 'CC',
				basicMarioArmatureData,
				blenderToSM64Rotation, useArmature = True)
	
			romfileSrc.close()
	
			return {'FINISHED'} # must return a set
		except:
			self.report({'ERROR'}, traceback.format_exc())
			romfileSrc.close()
			return {'CANCELLED'} # must return a set

class SM64_ImportLevel(bpy.types.Operator):
	# set bl_ properties
	bl_idname = 'object.sm64_import_lvl'
	bl_label = "Import Level"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	importRom = bpy.props.StringProperty(name ='Import ROM', subtype = 'FILE_PATH')
	levelLevel = bpy.props.EnumProperty(items = level_enums, 
		name = 'Level', default = 'CG')

	# Called on demand (i.e. button press, menu item)
	# Can also be called from operator search menu (Spacebar)
	def execute(self, context):
		try:
			romfileSrc = open(self.importRom, 'rb')
			levelParsed = parseLevelAtPointer(romfileSrc, level_pointers[self.levelLevel])
			segmentData = levelParsed.segmentData

			areaGeos = [parseGeoLayout(romfileSrc, decodeSegmentedAddr(
				area.geoAddress, segmentData), self.levelLevel, None,
				blenderToSM64RotationLevel, shadeSmooth=False) for area in levelParsed.areas]
	
			romfileSrc.close()
	
			return {'FINISHED'} # must return a set
		except:
			self.report({'ERROR'}, traceback.format_exc())
			romfileSrc.close()
			return {'CANCELLED'} # must return a set

class SM64_ExportLevel(bpy.types.Operator):
	# set bl_ properties
	bl_idname = 'object.sm64_export_lvl'
	bl_label = "Export Level"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	exportRom = bpy.props.StringProperty(name ='Export ROM', subtype = 'FILE_PATH')
	outputRom = bpy.props.StringProperty(name ='Output ROM', subtype = 'FILE_PATH')
	levelLevel = bpy.props.EnumProperty(items = level_enums, 
		name = 'Level', default = 'CG')

	def execute(self, context):

		try:
			romfileExport = open(self.exportRom, 'rb')
			shutil.copy(self.exportRom, self.outputRom)
			romfileExport.close()
			romfileOutput = open(self.outputRom, 'rb+')

			segmentData = {}
			ExtendBank0x04(romfileOutput, segmentData)
			DisableLowPolyMario(romfileOutput)
	
			allObjs = context.selected_objects
			bpy.ops.object.select_all(action = 'DESELECT')
			for obj in allObjs:
				if 'sm64_rom_start' in obj and \
					'sm64_rom_end' in obj and \
					'sm64_geolayout_pointers' in obj:
	
					start = obj['sm64_rom_start']
					end = obj['sm64_rom_end']
					geo = obj['sm64_geolayout_pointers']

					if 'sm64_draw_layer' in obj:
						drawLayer = obj['sm64_draw_layer']
					else:
						drawLayer = 0x01
	
					success = importGeneric(romfileOutput, obj, scene, 'Y', start, 
						end, levelEnum, geo, blenderToSM64Rotation,
						drawLayer, verbose = True)
					if not success:
						self.report({'ERROR'}, 'Model is too large.')
						romfileOutput.close()
						return {'CANCELLED'} # must return a set

					obj.select = True
	
				else:
					self.report({'ERROR'}, obj.name + \
						" does not have correct custom properties.")
	
	
			romfileOutput.close()
			self.report({'INFO'}, 'Export succeeded.')
			return {'FINISHED'} # must return a set

		except:
			romfileOutput.close()
			self.report({'ERROR'}, traceback.format_exc())
			return {'CANCELLED'} # must return a set

class SM64_ImportAnimMario(bpy.types.Operator):
	bl_idname = 'object.sm64_import_anim'
	bl_label = "Import Mario Animation"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	animStart = bpy.props.IntProperty(name ='Animation Start (base 10)', default = 5162640)
	importRom = bpy.props.StringProperty(name ='Import ROM', subtype = 'FILE_PATH')

	# Called on demand (i.e. button press, menu item)
	# Can also be called from operator search menu (Spacebar)
	def execute(self, context):
		romfileSrc = open(self.importRom, 'rb')

		# Note actual level doesn't matter for Mario, since he is in all of them
		#levelParsed = parseLevelAtPointer(romfileSrc, level_pointers['CC'])
		#segmentData = levelParsed.segmentData
		importAnimationToBlender(romfileSrc, self.animStart)

		return {'FINISHED'} # must return a set

class SM64_ImportExport_UI(bpy.types.Panel):
	bl_idname = "sm64_import_panel"
	bl_label = "SM64 Model Importer"
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'TOOLS'
	bl_category = 'Tools'

	# called every frame
	def draw(self, context):
		col = self.layout.column()
		col.prop(context.scene, 'importRom')
		col.prop(context.scene, 'exportRom')
		col.prop(context.scene, 'outputRom')
		col.separator()
		col.separator()
		col.separator()
		col.separator()

		col.label("Mario Model Importer/Exporter")
		propsI = col.operator(SM64_ImportMario.bl_idname)
		propsI.importRom = context.scene.importRom
		propsI.customHatless = context.scene.customHatless
		#col.prop(context.scene, "customHatless")

		propsE = col.operator(SM64_ExportMario.bl_idname)
		propsE.exportRom = context.scene.exportRom
		propsE.outputRom = context.scene.outputRom
		propsE.useFaceAnimation = context.scene.useFaceAnimation
		propsE.overwriteWingPositions = context.scene.overwriteWingPositions
		propsE.useCustomExportRange = context.scene.useCustomExportRange
		propsE.customExportRange = context.scene.customExportRange
		propsE.useLogFile = context.scene.useLogFile
		propsE.logFilePath = context.scene.logFilePath

		col.prop(context.scene, 'useFaceAnimation')
		col.prop(context.scene, 'overwriteWingPositions')
		col.prop(context.scene, 'useCustomExportRange')
		if context.scene.useCustomExportRange:
			col.prop(context.scene, 'customExportRange')
		# col.prop(context.scene, 'useLogFile')
		# if context.scene.useLogFile:
		#	col.prop(context.scene, 'logFilePath')

		col.separator()
		col.separator()
		col.separator()
		col.separator()

		col.label("Geolayout Importer (Experimental)")
		propsGeoI = col.operator(SM64_ImportGeolayout.bl_idname)

		propsGeoI.generateArmature = context.scene.generateArmature
		propsGeoI.geolayoutStart = context.scene.geolayoutStart
		propsGeoI.levelGeo = context.scene.levelGeo
		propsGeoI.importRom = context.scene.importRom

		col.prop(context.scene, 'generateArmature')
		col.prop(context.scene, 'geolayoutStart')
		col.prop(context.scene, 'levelGeo')

		col.separator()
		col.separator()
		col.separator()
		col.separator()

		col.label("Display List Importer/Exporter (Experimental)")
		propsDLI = col.operator(SM64_ImportDL.bl_idname)
		propsDLE = col.operator(SM64_ExportDL.bl_idname)

		propsDLI.displayListStart = context.scene.displayListStart
		propsDLI.levelDL = context.scene.levelDL
		propsDLI.importRom = context.scene.importRom
		col.prop(context.scene, 'displayListStart')

		propsDLE.exportRom = context.scene.exportRom
		propsDLE.outputRom = context.scene.outputRom
		propsDLE.levelDL = context.scene.levelDL
		col.prop(context.scene, 'levelDL')

		col.separator()
		col.separator()
		col.separator()
		col.separator()

		col.label("Level Importer (Experimental)")
		propsLI = col.operator(SM64_ImportLevel.bl_idname)
		#propsLE = col.operator(SM64_ExportLevel.bl_idname)

		propsLI.levelLevel = context.scene.levelLevel
		propsLI.importRom = context.scene.importRom
		col.prop(context.scene, 'levelLevel')

		'''
		propsLE.exportRom = context.scene.exportRom
		propsLE.outputRom = context.scene.outputRom
		propsLE.levelLevel = context.scene.levelLevel
		
		'''

		'''
		col.separator()
		col.separator()
		col.separator()
		col.separator()

		col.label("Mario Animation Importer/Exporter (Broken)")
		propsAnim = col.operator(SM64_ImportAnimMario.bl_idname)
		propsAnim.animStart = context.scene.animStart
		propsAnim.importRom = context.scene.importRom

		col.prop(context.scene, 'animStart')
		'''

		"""
		 self
		 -	layout (bpy.types.UILayout)
		 	- label() # works for row/column/etc. too
			- row() / column() / column_flow()
				- menu() # creates a menu layout
				- prop(data = context.scene) 
					# exposes RNA item (has enum and menu_enum alt functions)
				- operator() # adds button to call operator
				- separator() # add line break

		RNA/DNA are blender serialization formats.

		"""

	# Returns None if panel shouldn't be drawn
	# This is a class method
	@classmethod
	def poll(cls, context):
		return True

# called on add-on enabling
# register operators and panels here
# append menu layout drawing function to an existing window
def register():
	bpy.utils.register_class(SM64_ImportExport_UI)
	bpy.utils.register_class(SM64_ImportMario)
	bpy.utils.register_class(SM64_ExportMario)
	bpy.utils.register_class(SM64_ImportGeolayout)
	bpy.utils.register_class(SM64_ImportDL)
	bpy.utils.register_class(SM64_ExportDL)
	bpy.utils.register_class(SM64_ImportAnimMario)
	bpy.utils.register_class(SM64_ImportLevel)
	bpy.utils.register_class(SM64_ExportLevel)

	bpy.types.Scene.customHatless = bpy.props.BoolProperty(
		name ='Also import hatless head?')
	bpy.types.Scene.useFaceAnimation = bpy.props.BoolProperty(
		name ='Use Face Animation?')
	bpy.types.Scene.overwriteWingPositions = bpy.props.BoolProperty(
		name ='Overwrite Wing Positions? (See attatched diagram)')
	bpy.types.Scene.generateArmature = bpy.props.BoolProperty(
		name ='Generate Armature?')
	bpy.types.Scene.geolayoutStart = bpy.props.IntProperty(
		name ='Geolayout Start (base 10)', default = 2039136)
	bpy.types.Scene.displayListStart = bpy.props.IntProperty(
		name ='Import Display List Start (base 10)', default = 0xA3BE1C)
	bpy.types.Scene.levelGeo = bpy.props.EnumProperty(items = level_enums, 
		name = 'Level Used By Geolayout', default = 'HMC')
	bpy.types.Scene.levelDL = bpy.props.EnumProperty(items = level_enums, 
		name = 'Level Used By Display List', default = 'CG')
	bpy.types.Scene.levelLevel = bpy.props.EnumProperty(items = level_enums, 
		name = 'Level', default = 'CG')
	bpy.types.Scene.animStart = bpy.props.IntProperty(name ='Animation Start (base 10)', default = 0)
	bpy.types.Scene.useCustomExportRange = bpy.props.BoolProperty(name ='Use custom export range? (Bank 0x04 will not be extended)')
	bpy.types.Scene.customExportRange = bpy.props.IntVectorProperty(name="Custom export range", 
		default=(0x823B64, 0x858EDC), size = 2)
	bpy.types.Scene.useLogFile = bpy.props.BoolProperty(name ='Write log file?')
	bpy.types.Scene.logFilePath = bpy.props.StringProperty(name = 'Log File', subtype = 'FILE_PATH')

	bpy.types.Scene.importRom = bpy.props.StringProperty(
		name ='Import ROM', subtype = 'FILE_PATH')
	bpy.types.Scene.exportRom = bpy.props.StringProperty(
		name ='Export ROM', subtype = 'FILE_PATH')
	bpy.types.Scene.outputRom = bpy.props.StringProperty(
		name ='Output ROM', subtype = 'FILE_PATH')
	
# called on add-on disabling
def unregister():
	del bpy.types.Scene.customHatless
	del bpy.types.Scene.useFaceAnimation
	del bpy.types.Scene.overwriteWingPositions
	del bpy.types.Scene.generateArmature
	del bpy.types.Scene.geolayoutStart
	del bpy.types.Scene.levelGeo
	del bpy.types.Scene.levelDL
	del bpy.types.Scene.levelLevel
	del bpy.types.Scene.animStart
	del bpy.types.Scene.useCustomExportRange
	del bpy.types.Scene.customExportRange
	del bpy.types.Scene.useLogFile
	del bpy.types.Scene.logFilePath

	del bpy.types.Scene.importRom
	del bpy.types.Scene.exportRom
	del bpy.types.Scene.outputRom

	bpy.utils.unregister_class(SM64_ImportExport_UI)
	bpy.utils.unregister_class(SM64_ImportMario)
	bpy.utils.unregister_class(SM64_ExportMario)
	bpy.utils.unregister_class(SM64_ImportGeolayout)
	bpy.utils.unregister_class(SM64_ImportDL)
	bpy.utils.unregister_class(SM64_ExportDL)
	bpy.utils.unregister_class(SM64_ImportAnimMario)
	bpy.utils.unregister_class(SM64_ImportLevel)
	bpy.utils.unregister_class(SM64_ExportLevel)