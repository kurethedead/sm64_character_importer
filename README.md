
# SM64 Blender Importer  
# NOTE: This plugin is deprecated. Use [fast64](https://bitbucket.org/kurethedead/fast64/src/master/) instead.
![alt-text](https://bitbucket.org/kurethedead/sm64_character_importer/raw/master/screenshots/blender.png)


This is a Blender plugin that allows one to import custom Mario models, without having to manually import each body part separately. It also lets one import/export custom display lists. It relies on Blender custom data attributes that determine how the mesh in Blender is imported.  

### Features (import means import into Blender)  
-   Mario import/export 
-   Modified bone lengths are saved, excluding the root bone  
-   Face animation supported  
-   Can import custom hatless head/other hand poses  
-   Will update metal/vanish models 
-   Geo layout import  
-   Display List import/export  
-   Custom color combination functionality 
-   Vertex painting  

### Restrictions
-   Textures are not imported, only exported  
-   The Blender custom data attributes are necessary for the plugin to work; Make sure to follow the process outlined in the instruction files.  

![alt-text](https://bitbucket.org/kurethedead/sm64_character_importer/raw/master/screenshots/mario%20hat.gif) ![alt-text](https://bitbucket.org/kurethedead/sm64_character_importer/raw/master/screenshots/mario%20wings.gif)
