from .sm64_data import *

def ExtendBank0x04(romfile, segmentData):

	# Extend bank 0x04
	romfile.seek(loadSegmentAddresses[0x04] + 4)
	oldStart = int.from_bytes(romfile.read(4), 'big')
	romfile.seek(loadSegmentAddresses[0x04] + 4)
	romfile.write(bytearray.fromhex('01 1A 35 B8'))

	romfile.seek(loadSegmentAddresses[0x04] + 8)
	oldEnd = int.from_bytes(romfile.read(4), 'big')
	romfile.seek(loadSegmentAddresses[0x04] + 8)
	romfile.write(bytearray.fromhex('01 1F FF 00'))

	romfile.seek(oldStart)
	oldData = romfile.read(oldEnd - oldStart)

	romfile.seek(0x11A35B8)
	romfile.write(oldData)

	segmentData[0x04] = (0x011A35B8, 0x011FFF00)

def DisableLowPolyMario(romfile):
	# disable low poly mario
	romfile.seek(0x12A7C6)
	romfile.write(bytearray.fromhex('2E 18'))

