import sys
import os
import string

def decodeF5(hexString):
	bitString = format(int(hexString, 16), '064b')

	colorFormat  			= int(bitString[ 8:11], 2)
	pixelBitSize 			= int(bitString[11:13], 2)
	num64BitValuesPerRow	= int(bitString[14:23], 2)
	offsetTMEM				= int(bitString[23:32], 2)
	tileID 					= int(bitString[37:40], 2)
	colorPallette 			= int(bitString[40:44], 2)
	tClampMirrorFlags		= int(bitString[44:46], 2)	
	tAxisWrapExtent			= int(bitString[46:50], 2)
	tAxisPerspectiveShift 	= int(bitString[50:54], 2)		
	sClampMirrorFlags		= int(bitString[54:56], 2)	
	sAxisWrapExtent			= int(bitString[56:60], 2)
	sAxisPerspectiveShift	= int(bitString[60:64], 2)

	return (colorFormat, pixelBitSize, 
		num64BitValuesPerRow, offsetTMEM, tileID, colorPallette, 
		tClampMirrorFlags, tAxisWrapExtent, tAxisPerspectiveShift, 
		sClampMirrorFlags, sAxisWrapExtent, sAxisPerspectiveShift)

def encodeF5(colorFormat, 
	pixelBitSize, num64BitValuesPerRow, offsetTMEM, tileID, colorPallette, 
	tClampMirrorFlags, tAxisWrapExtent, tAxisPerspectiveShift, 
	sClampMirrorFlags, sAxisWrapExtent, sAxisPerspectiveShift):
	
	command = 0xF5
	command = command << 3 | colorFormat
	command = command << 2 | pixelBitSize
	command = command << 1
	command = command << 9 | int(num64BitValuesPerRow)
	command = command << 9 | offsetTMEM
	command = command << 5
	command = command << 3 | tileID
	command = command << 4 | colorPallette
	command = command << 2 | tClampMirrorFlags
	command = command << 4 | tAxisWrapExtent
	command = command << 4 | tAxisPerspectiveShift
	command = command << 2 | sClampMirrorFlags
	command = command << 4 | sAxisWrapExtent
	command = command << 4 | sAxisPerspectiveShift

	return command.to_bytes(8, 'big')

def decodeFC(input):
	# Convert hex to decimal, then format as a binary string 
	# using 64 digits and without the leading '0b'.
	decimalValue = int(input,16)
	bitString = format(decimalValue, '064b')
	
	# {start, end]
	a = int(bitString[ 8: 12], 2)
	c = int(bitString[12: 17], 2)
	e = int(bitString[17: 20], 2)
	g = int(bitString[20: 23], 2)
	i = int(bitString[23: 27], 2)
	k = int(bitString[27: 32], 2)
	b = int(bitString[32: 36], 2)
	j = int(bitString[36: 40], 2)
	m = int(bitString[40: 43], 2)
	o = int(bitString[43: 46], 2)
	d = int(bitString[46: 49], 2)
	f = int(bitString[49: 52], 2)
	h = int(bitString[52: 55], 2)
	l = int(bitString[55: 58], 2)
	n = int(bitString[58: 61], 2)
	p = int(bitString[61: 64], 2)

	return(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p)

def encodeFC(a, b, c, d, e, f, g, h,
			i, j, k, l, m, n, o, p):
	FCcommand = ""
	FCcommand += createBitString(colorSources[a], 4) #a
	FCcommand += createBitString(colorSources[c], 5) #c
	FCcommand += createBitString(alphaSources[e], 3) #e
	FCcommand += createBitString(alphaSources[g], 3) #g
	FCcommand += createBitString(colorSources[i], 4) #i
	FCcommand += createBitString(colorSources[k], 5) #k
	FCcommand += createBitString(colorSources[b], 4) #b
	FCcommand += createBitString(colorSources[j], 4) #j
	FCcommand += createBitString(alphaSources[m], 3) #m
	FCcommand += createBitString(alphaSources[o], 3) #o
	FCcommand += createBitString(colorSources[d], 3) #d
	FCcommand += createBitString(alphaSources[f], 3) #f
	FCcommand += createBitString(alphaSources[h], 3) #h
	FCcommand += createBitString(colorSources[l], 3) #l
	FCcommand += createBitString(alphaSources[n], 3) #n
	FCcommand += createBitString(alphaSources[p], 3) #p

	return bytearray([0xFC]) + int(FCcommand, 2).to_bytes(7, 'big')

# get binary substring and convert it to a decimal
def binarySubstring(binaryString, start, end):
	value = int(binaryString[start:end], 2) 
	#print(value)
	return value

def createBitString(value, bitSize):
	bitString = format(value, '0' + str(bitSize) + 'b')
	if len(bitString) > bitSize:
		# slice off highest (left most) bits to fit size
		bitString = bitString[len(bitString) - bitSize :]
	return bitString

# Color combination options.
colorSources = {
	'combined' :	 	0,
	'texel0' :		 	1,
	'texel1' :		 	2,
	'primitive' :	 	3,
	'shade' : 		 	4,
	'environment' :  	5,
	'center' : 		 	6,
	'scale' : 		 	6,
	'combined_alpha':	7,
	'texel0_alpha' : 	8,
	'texel1_alpha' : 	9,
	'primitive_alpha' : 10,
	'shade_alpha' : 	11,
	'env_alpha' : 		12,
	'lod_fraction' : 	13,
	'prim_lod_frac' : 	14,
	'noise' : 			7,
	'k4' : 				7,
	'k5' : 				15,
	'1' : 				6,
	'0' : 				31,
}

alphaSources = {
	'combined'		: 0,
	'texel0'		: 1,
	'texel1'		: 2,
	'primitive'		: 3,
	'shade'			: 4,
	'environment'	: 5,
	'lod_fraction'	: 0,
	'prim_lod_frac'	: 6,
	'1'				: 6,
	'0'				: 7,
}

# Color combination: (A - B) * C + D

def inputA(n):
	if n == 0:
		return "COMBINED"
	elif n == 1:
		return "TEXEL0"
	elif n == 2:
		return "TEXEL1"
	elif n == 3:
		return "PRIMITIVE"
	elif n == 4:
		return "SHADE"
	elif n == 5:
		return "ENVIRONMENT"
	elif n == 6:
		return "1.0"
	elif n == 7:
		return "NOISE"
	elif n in range(8,16):
		return "0.0"
	else:
		return "ERROR"

def inputB(n):
	if n == 0:
		return "COMBINED"
	elif n == 1:
		return "TEXEL0"
	elif n == 2:
		return "TEXEL1"
	elif n == 3:
		return "PRIMITIVE"
	elif n == 4:
		return "SHADE"
	elif n == 5:
		return "ENVIRONMENT"
	elif n == 6:
		return "CENTER"
	elif n == 7:
		return "K4"
	elif n in range(8,16):
		return "0.0"
	else:
		return "ERROR"

def inputC(n):
	if n == 0:
		return "COMBINED"
	elif n == 1:
		return "TEXEL0"
	elif n == 2:
		return "TEXEL1"
	elif n == 3:
		return "PRIMITIVE"
	elif n == 4:
		return "SHADE"
	elif n == 5:
		return "ENVIRONMENT"
	elif n == 6:
		return "SCALE"
	elif n == 7:
		return "COMBINED_ALPHA"
	elif n == 8:
		return "TEXEL0_ALPHA"
	elif n == 9:
		return "TEXEL1_ALPHA"
	elif n == 10:
		return "PRIMITIVE_ALPHA"
	elif n == 11:
		return "SHADED_ALPHA"
	elif n == 12:
		return "ENV_ALPHA"
	elif n == 13:
		return "LOD_FRACTION"
	elif n == 14:
		return "PRIM_LOD_FRAC"
	elif n == 15:
		return "K5"
	elif n in range(16,32):
		return "0.0"
	else:
		return "ERROR"

def inputD(n):
	if n == 0:
		return "COMBINED"
	elif n == 1:
		return "TEXEL0"
	elif n == 2:
		return "TEXEL1"
	elif n == 3:
		return "PRIMITIVE"
	elif n == 4:
		return "SHADE"
	elif n == 5:
		return "ENVIRONMENT"
	elif n == 6:
		return "1.0"
	elif n == 7:
		return "0.0"
	else:
		return "ERROR"

# Alpha combination: (E - F) * G + H

def inputG(n):
	if n == 0:
		return "LOD_FRACTION"
	elif n == 1:
		return "TEXEL0_ALPHA"
	elif n == 2:
		return "TEXEL1_ALPHA"
	elif n == 3:
		return "PRIMITIVE_ALPHA"
	elif n == 4:
		return "SHADE_ALPHA"
	elif n == 5:
		return "ENV_ALPHA"
	elif n == 6:
		return "PRIM_LOD_FRAC"
	elif n == 7:
		return "0.0"
	else:
		return "ERROR"

def inputEFH(n):
	if n == 0:
		return "COMBINED_ALPHA"
	elif n == 1:
		return "TEXEL0_ALPHA"
	elif n == 2:
		return "TEXEL1_ALPHA"
	elif n == 3:
		return "PRIMITIVE_ALPHA"
	elif n == 4:
		return "SHADE_ALPHA"
	elif n == 5:
		return "ENV_ALPHA"
	elif n == 6:
		return "1.0"
	elif n == 7:
		return "0.0"
	else:
		return "ERROR"