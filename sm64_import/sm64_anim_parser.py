import bpy
import mathutils
from .sm64_utility import *
from .sm64_data import *
from math import pi

sm64_anim_types = {'ROTATE', 'TRANSLATE'}

class SM64_AnimationMetadata:
	def __init__(self, repetitions, numFrames, nodeCount):
		self.repetitions = repetitions
		self.numFrames = numFrames
		self.nodeCount = nodeCount
		self.transformIndices = []

class SM64_AnimIndexNode:
	def __init__(self, x, y, z):
		self.x = x
		self.y = y
		self.z = z

class SM64_AnimIndexValue:
	def __init__(self, numFrames, index):
		self.index = index
		self.numFrames = numFrames


def importAnimationToBlender(romfile, startAddress):
	animationMetadata, armatureFrameData = \
		readAnimation(romfile, startAddress)

	bpy.context.scene.render.fps = 30
	bpy.context.scene.frame_end = max(
		bpy.context.scene.frame_end, 
		animationMetadata.numFrames)
	anim = bpy.data.actions.new("sm64_anim")

	boneIndex = 0
	isRootTranslation = True
	isRootRotation = False
	for boneFrameData in armatureFrameData:
		vectorName = 'location' if isRootTranslation else 'rotation_euler'
		propertyIndex = 0
		for frames in boneFrameData:
			if isRootTranslation:
				fcurve = anim.fcurves.new(
					data_path = 'location',
					index = propertyIndex,
					action_group = 'root')
			elif isRootRotation:
				fcurve = anim.fcurves.new(
					data_path = 'rotation_euler',
					index = propertyIndex,
					action_group = 'root')
			else:
				fcurve = anim.fcurves.new(
					data_path = 'pose.bones["' + basicMarioArmatureData.bones[boneIndex].name + \
						'"].' + vectorName, 
					index = propertyIndex,
					action_group = basicMarioArmatureData.bones[boneIndex].name)
	
			framecount = 0
			for frameValue in frames:
				fcurve.keyframe_points.insert(framecount, frameValue)
				framecount += 1
	
			propertyIndex += 1

		if boneIndex < len(basicMarioArmatureData.bones) - 1:
			boneIndex += 1

		isRootRotation = False
		if isRootTranslation:
			isRootRotation = True
		isRootTranslation = False

def readAnimation(romfile, startAddress):
	numRepeats, numFrames, numNodes, \
	transformValuesStart, transformIndicesStart, animDataLength = \
		readAnimHeader(romfile, startAddress)

	animationMetadata = SM64_AnimationMetadata(numRepeats, numFrames, numNodes)
	animationMetadata.transformIndices = readAnimIndices(
		romfile, transformIndicesStart, numFrames)

	armatureFrameData = [] #list of list of frames
	isRootTranslation = True
	isRootRotation = False
	for boneIndex in animationMetadata.transformIndices:
		boneFrameData = [[],[],[]]

		anim_type = 'TRANSLATE' if isRootTranslation else 'ROTATE'

		for framecount in range(0, numFrames):
			x = evaluateKeyFrame(romfile, transformValuesStart, 
				framecount, boneIndex.x, anim_type)
			y = evaluateKeyFrame(romfile, transformValuesStart, 
				framecount, boneIndex.y, anim_type)
			z = evaluateKeyFrame(romfile, transformValuesStart, 
				framecount, boneIndex.z, anim_type)

			if isRootTranslation:
				value = mathutils.Vector((x,y,z))
				value = blenderToSM64Rotation.inverted().to_quaternion() * value

				boneFrameData[0].append(value[0])
				boneFrameData[1].append(value[1])
				boneFrameData[2].append(value[2])
			else:
				value = mathutils.Euler((x,y,z))
				if isRootRotation:
					value.rotate(blenderToSM64Rotation.inverted().to_quaternion())

				boneFrameData[0].append(value.x)
				boneFrameData[1].append(value.y)
				boneFrameData[2].append(value.z)

		armatureFrameData.append(boneFrameData)
		isRootRotation = False
		if isRootTranslation:
			isRootRotation = True
		isRootTranslation = False

	return (animationMetadata, armatureFrameData)

def evaluateKeyFrame(romfile, transformValuesStart, 
	framecount, valueIndex, anim_type):
	key = framecount \
		if framecount < valueIndex.numFrames \
		else valueIndex.numFrames - 1
	ptrToValue = transformValuesStart + 2 * key + 2 * valueIndex.index 

	romfile.seek(ptrToValue)
	if anim_type == 'ROTATE':
		return int.from_bytes(romfile.read(2), 'big') *\
			2 * pi / (2**16 - 1)
	elif anim_type == 'TRANSLATE':
		return int.from_bytes(romfile.read(2), 'big', signed = True) *\
			sm64ToBlenderScale
	else:
		raise ValueError('Invalid anim_type: ' + anim_type)

def readAnimHeader(romfile, startAddress):
	romfile.seek(startAddress + 0x00)
	numRepeats = int.from_bytes(romfile.read(2), 'big')

	romfile.seek(startAddress + 0x08)
	numFrames = int.from_bytes(romfile.read(2), 'big')

	romfile.seek(startAddress + 0x0A)
	numNodes = int.from_bytes(romfile.read(2), 'big')

	romfile.seek(startAddress + 0x0C)
	transformValuesOffset = int.from_bytes(romfile.read(4), 'big')
	transformValuesStart = startAddress + transformValuesOffset

	romfile.seek(startAddress + 0x10)
	transformIndicesOffset = int.from_bytes(romfile.read(4), 'big')
	transformIndicesStart = startAddress + transformIndicesOffset

	romfile.seek(startAddress + 0x14)
	animDataLength = int.from_bytes(romfile.read(4), 'big')

	return (numRepeats, numFrames, numNodes, 
		transformValuesStart, transformIndicesStart,
		animDataLength)

def readAnimIndices(romfile, ptrAddress, nodeCount):
	currentAddress = ptrAddress
	indices = []

	rootPosIndex = readTransformIndex(romfile, currentAddress)
	indices.append(rootPosIndex)
	currentAddress += 12

	for i in range(nodeCount):
		rotationIndex = readTransformIndex(romfile, currentAddress)
		indices.append(rotationIndex)
		currentAddress += 12

	return indices

def readTransformIndex(romfile, startAddress):
	x = readValueIndex(romfile, startAddress + 0)
	y = readValueIndex(romfile, startAddress + 4)
	z = readValueIndex(romfile, startAddress + 8)

	return SM64_AnimIndexNode(x, y, z)

def readValueIndex(romfile, startAddress):
	romfile.seek(startAddress)
	numFrames = int.from_bytes(romfile.read(2), 'big')
	romfile.seek(startAddress + 2)
	index = int.from_bytes(romfile.read(2), 'big')

	return SM64_AnimIndexValue(numFrames, index)

def writeAnimation(romfile, startAddress, segmentData):
	pass

def writeAnimHeader(romfile, startAddress, segmentData):
	pass