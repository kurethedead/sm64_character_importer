import bpy
import mathutils
import math
import copy

from math import pi

from .sm64_level_parser import *
from .sm64_geolayout_macros import *
from .sm64_f3d_parser import *
from .sm64_utility import *
from .sm64_data import *

# does NOT create new geolayout, but modifies any 
# translation/rotation/scale on existing geolayout.
def saveGeolayoutTransforms(romfile, armatureObj, geoStartAddress, 
	armatureData, convertTransformMatrix, ignoreParts = []):
	boneOffsetDict = dict([(bone.geo_pointer_relative, bone.name) for \
						bone in armatureData.bones if bone.name not in ignoreParts])
	zeroTranslation = bytearray(6 * [0x00])

	if 'sm64_global_scale' in armatureObj:
		globalScaleOffset = armatureData.globalScaleAddrOffset
		if globalScaleOffset is None:
			print ("Warning: Found sm64_global_scale property on armature, but the" +\
				'corresponding field in the armatureData is not set.')
		else:
			romfile.seek(geoStartAddress + globalScaleOffset + 4)
			romfile.write(int(armatureObj['sm64_global_scale'] \
				* 0x10000).to_bytes(4, 'big'))

	for bone in armatureObj.data.bones:

		if not 'sm64_geo_meta' in bone:
			continue
		
		if 'normal wing' in bone.name and ('normal wing 2a' in ignoreParts \
			or 'normal wing 2b' in ignoreParts):
			continue

		ancestorWithGeoAddr = bone
		hasGeoAddr = False

		while not hasGeoAddr:
			ancestorWithGeoAddr = ancestorWithGeoAddr.parent
			if ancestorWithGeoAddr is None:
				break
			hasGeoAddr = 'sm64_geo_meta' in ancestorWithGeoAddr

		if ancestorWithGeoAddr is not None:
			translationLocal = convertTransformMatrix * \
				(bone.head_local - ancestorWithGeoAddr.head_local)
		else:
			translationLocal = convertTransformMatrix * \
				bone.head_local

		translationWritten = False

		if 'geo_trans_ptr' in bone['sm64_geo_meta']:
			translationLocal = convertTransformMatrix * \
				(bone.head_local - bone.parent.head_local)

		#	# apply inverse rotation to current translation
		#	romfile.seek(bone['sm64_geo_meta']['geo_trans_ptr'])
		#	command = romfile.read(16)
		#	eulerAngles = readEulerVectorFromShorts(command, 10, signed = False)
		#	rotation = mathutils.Quaternion([math.radians(val) for val in eulerAngles])
		#	translationLocal = rotation.inverted() * translationLocal

		translation = convertPosition(translationLocal)
		useOverride = True

		# Handle top level overlap
		topOverlap = bone['sm64_geo_meta']['geo_top_overlap_ptr']

		if topOverlap is not None:
			armatureData.applyTranslationGivenOffset(
				romfile, geoStartAddress, translation, topOverlap, useOverride)
			translationWritten = True

		# Handle intermediate overlaps
		for overlapRelativeAddr in bone['sm64_geo_meta']['geo_other_overlap_ptrs']:
			armatureData.applyTranslationGivenOffset(
				romfile, geoStartAddress, zeroTranslation, overlapRelativeAddr, useOverride)

		# Handle current pointer
		geoPtrRelative = bone['sm64_geo_meta']['geo_pointer_relative']
		if translationWritten:
			armatureData.applyTranslationGivenOffset(
				romfile, geoStartAddress, zeroTranslation, geoPtrRelative, useOverride)
		else:
			armatureData.applyTranslationGivenOffset(
				romfile, geoStartAddress, translation, geoPtrRelative, useOverride)
			translationWritten = True
			
		# Remove from list.
		if bone['sm64_geo_meta']['geo_has_mesh']:
			boneOffsetDict.pop(geoPtrRelative, None)

	# Zero out any unhandled bones from list.
	for relativeOffset, bodyPart in boneOffsetDict.items():
		print(bodyPart)
		romfile.seek(geoStartAddress + relativeOffset)
		command = romfile.read(1)
		if command[0] == GEO_LOAD_DL_W_OFFSET and relativeOffset == topOverlap:
			pass

		# write zero dl
		armatureData.applyDLPointerGivenOffset(
			romfile, geoStartAddress, bytearray(4 * [0x00]), 
			bytearray(4 * [0x00]), relativeOffset, True)
