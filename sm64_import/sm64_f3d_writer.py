import pprint
import copy
import mathutils
from .sm64_level_parser import *
from .sm64_f3d_functions import *
from .sm64_f3d_parser import *
from .sm64_utility import *
from .sm64_data import *
from .sm64_f3d_classes import *

# Assume single diffuse light.
numDiffuseLights = 1
maxDiffuseLights = 7

# use one tile id for all texture for now
defaultTileId = 0

# white diffuse, grey ambient, normal = (1,1,1)
defaultLighting = [
	(mathutils.Vector((1,1,1)), mathutils.Vector((1, 1, 1)).normalized()),
	(mathutils.Vector((0.5, 0.5, 0.5)), mathutils.Vector((1, 1, 1)).normalized())]

def getSM64Material(b2fMaterial):
	blenderMaterial = b2fMaterial.material
	materialCode = bytearray(0)

	# Start with E7
	materialCode += RDPSync('pipe')

	if blenderMaterial is None:
		#materialCode += SetGeometryMode(G_NONE)
		materialCode += SetColorCombination(*S_SHADED_SOLID)
		print("Using shaded solid combo.")
		materialCode += TextureScaleFactor('standard')
		materialCode += RDPSync('load')
		return materialCode, b2fMaterial.setGeoFlags, b2fMaterial.clearGeoFlags

	if 'sm64_other_mode' in blenderMaterial:
		materialCode += SetOtherModeH(0, 24,
			# High Bits
			G_AD_PATTERN | G_CD_ENABLE | G_CK_NONE | \
			G_TC_CONV | G_TF_POINT | G_TT_NONE | \
			G_TL_TILE | G_TD_CLAMP | G_TP_PERSP | \
			G_CYC_2CYCLE | G_CD_ENABLE | G_PM_NPRIMITIVE
		)

	if 'sm64_setenv' in blenderMaterial:
		materialCode += SetEnvironmentColor(blenderMaterial['sm64_setenv'])

	if 'sm64_setprim' in blenderMaterial:
		materialCode += SetPrimColor(blenderMaterial['sm64_setprim'])

	if 'sm64_setfog' in blenderMaterial:
		materialCode += SetFogColor(blenderMaterial['sm64_setfog'])

	# handle color combination
	if 'sm64_cc' in blenderMaterial:
		try:
			materialCode += SetColorCombination(
				*blenderMaterial['sm64_cc'])
			#print("Successfully read custom color combo.")
		except:
			print('Invalid custom color combo, using shaded texture.')
			materialCode += SetColorCombination(*S_SHADED_TEX_CUTOUT)
	elif not isinstance(b2fMaterial, B2F_Material_Animated) and \
		b2fMaterial.texture0 is None and b2fMaterial.texture1 is None:
		# Converting untextured colored material to colored lighting cc
		materialCode += SetColorCombination(*S_SHADED_SOLID)
	else:
		materialCode += SetColorCombination(*S_SHADED_TEX_CUTOUT)
		print("Using shaded texture combo.")

	# Done this way since B2F_Material needs these properties too.
	if b2fMaterial.setGeoFlags != 0:
		materialCode += SetGeometryMode(b2fMaterial.setGeoFlags)
	if b2fMaterial.clearGeoFlags != 0:
		materialCode += ClearGeometryMode(b2fMaterial.clearGeoFlags)

	# Handle texture scale type
	if 'sm64_tex_scale' in blenderMaterial:
		result = TextureScaleFactor(blenderMaterial['sm64_tex_scale'])
		if result is None:
			print('Invalid texture scale type, using standard.')
			result = TextureScaleFactor('standard')
		materialCode += result
	else:
		materialCode += TextureScaleFactor('standard')

	return materialCode, b2fMaterial.setGeoFlags, b2fMaterial.clearGeoFlags

def generateMicrocode(romfile, partObj, scene, marioForwardAxis, 
	bodyParts, convertTransformMatrix, sm64_model, useAnim, isOverride, verbose = True, blenderOp = None):
	hasFaceAnim = False
	obj = B2F_Obj(partObj, scene, marioForwardAxis, False, convertTransformMatrix, useAnim)

	for mesh in obj.mesh_list:
		hasFaceAnim = hasFaceAnim or saveMesh(bodyParts, mesh, sm64_model, scene, isOverride)
		
	return hasFaceAnim

def addDrawLayerInfo(drawLayerDict, partObj):
	if 'sm64_part_names' not in partObj:
		return
	elif 'sm64_draw_layer' in partObj:
		for bodyPartName in partObj['sm64_part_names']:
			drawLayerDict[bodyPartName] = partObj['sm64_draw_layer']

def importMario(bodyParts, blenderOp, romfileOutput, context, marioForwardAxis, segmentData, armatureObj,
	startAddress, endAddress, geoStartAddress):
	sm64_model = SM64_uModel(segmentData, True)
	drawLayers = {}
	# [{start : xxx, name : xxx, size : xxx}]
	# partSizeList = {}
	for partObj in bodyParts:
		print(partObj.name)
		isAnimatedFace = blenderOp.useFaceAnimation and 'head (cap)' in partObj['sm64_part_names']
		if not 'sm64_part_names' in partObj:
			blenderOp.report({'ERROR'}, partObj.name + ' does not have a sm64_part_name property.')
			continue
		elif isAnimatedFace:
			print("Skipping static face...")
			continue
		elif 'normal wing 2a' in partObj['sm64_part_names'] or \
			'normal wing 2b' in partObj['sm64_part_names']:
			print("Skipping wings...")
			continue

		addDrawLayerInfo(drawLayers, partObj)
	
		bpy.ops.object.select_all(action = 'DESELECT')
		partObj.select = True
	
		generateMicrocode(romfileOutput, partObj, context.scene, 'Y',
			partObj['sm64_part_names'], blenderToSM64Rotation, sm64_model, 
				isAnimatedFace, False, True, blenderOp)

	if blenderOp.useFaceAnimation:
		faceObjs = [obj for obj in bodyParts if \
			'sm64_part_names' in obj and \
			'head (cap)' in obj['sm64_part_names']]
		if len(faceObjs) == 0:
			blenderOp.report({'ERROR'}, 'No mesh connected to armature contains' +\
				'"head (cap)" in its "sm64_part_names" property.')
		else:
			faceObj = faceObjs[0]
			bpy.ops.object.select_all(action = 'DESELECT')
			faceObj.select = True

			hasFaceAnim = generateMicrocode(romfileOutput, faceObj, context.scene, 'Y',
				faceObj['sm64_part_names'], blenderToSM64Rotation, sm64_model, 
				True, False, True, blenderOp)

			if not hasFaceAnim:
				printBlenderMessage({'ERROR'}, 'Trying to export model with face animations, ' +\
					'but no material with \"sm64_anim_mat\" property found.', blenderOp)
				#return False

			addDrawLayerInfo(drawLayers, faceObj)

	# handle additional body parts.
	for partObj in armatureObj.children:
		bpy.ops.object.select_all(action = 'DESELECT')
		partObj.select = True

		if 'sm64_part_names' not in partObj:
			blenderOp.report({'ERROR'}, 'Armature object child does not have an \'sm64_part_names\' custom property.')
			continue

		if partObj['sm64_part_names'][0] == 'head (no cap)' and blenderOp.useFaceAnimation:
			generateMicrocode(romfileOutput, partObj, context.scene, 'Y',
				partObj['sm64_part_names'], blenderToSM64Rotation, sm64_model, 
				True, True, True, blenderOp)
		else:
			generateMicrocode(romfileOutput, partObj, context.scene, 'Y',
				partObj['sm64_part_names'], blenderToSM64Rotation, sm64_model,
				False, True, True, blenderOp)

		addDrawLayerInfo(drawLayers, partObj)

	for partObjName, drawLayer in drawLayers.items():
		print(partObjName + " " + str(drawLayer))

	# set contiguous doesnt work
	# need to check size limits
	# fix updateGeolayout and layers etc.
	finalAddress = sm64_model.setContiguous(startAddress)
	if finalAddress > endAddress:
		printBlenderMessage({'ERROR'}, "Model exceeds max size of " + format(endAddress - startAddress, '#08x') + \
			".", blenderOp)
		print("Size: " + str(finalAddress - startAddress) + " of " + \
			str(endAddress - startAddress) + " bytes attempted to be used.")
		return False
	else:
		sm64_model.applyRelativeAddresses()
		sm64_model.writeToFile(romfileOutput)
		updateMarioGeolayoutPtrsAndLayers(sm64_model, geoStartAddress, 
			drawLayers, romfileOutput, blenderOp.useFaceAnimation)	

		print("Size: " + str(finalAddress - startAddress) + " of " + \
			str(endAddress - startAddress) + " bytes used.")
		print("Final Address: " + str(finalAddress))	
		return True

def importGeneric(romfile, partObj, scene, marioForwardAxis, romStart, 
	romEnd, levelEnum, geoPointerAddresses, convertTransformMatrix,
	drawLayer = 0x01, verbose = True):
	levelParsed = parseLevelAtPointer(romfile, level_pointers[levelEnum])
	segmentData = levelParsed.segmentData
	facePtrCount = 0

	obj = B2F_Obj(partObj, scene, marioForwardAxis, False, convertTransformMatrix, False)
	sm64_model = SM64_uModel(segmentData, False)

	for mesh in obj.mesh_list:
		facePtrCount += saveMesh(partObj.name, mesh, sm64_model, scene, False)

	finalAddr = sm64_model.setContiguous(romStart)
	if finalAddr > romEnd:
		print("Model exceeds max size of " + format(romEnd - romStart, '#08x') + \
			". Breakdown:")
		print("Size: " + str(finalAddr - romStart) + " of " + \
			str(romEnd - romStart) + " bytes attempted to be used.")
		return False
	else :
		sm64_model.applyRelativeAddresses()
		sm64_model.writeToFile(romfile)
		updateGenericGeolayoutPtrsAndLayers(sm64_model, geoPointerAddresses, \
			drawLayer, romfile)

		print("Size: " + str(finalAddr - romStart) + " of " + \
			str(romEnd - romStart) + " bytes used.")
		print("Final Address: " + str(finalAddr))	
		return True	

#return bytes
def saveMesh(bodyParts, mesh, sm64_model, scene, isOverride):
	isAnimMesh = False
	for material in mesh.materials:
		isAnimMesh = isAnimMesh or isinstance(material, B2F_Material_Animated)

	if isAnimMesh:
		sm64_mesh = SM64_uTexAnimMesh(sm64_model.levelData, marioFaceExpressionCount, sm64_model.useMetalShader)
	else:
		sm64_mesh = SM64_uMesh(sm64_model.levelData, sm64_model.useMetalShader)
	if sm64_model.useMetalShader:
		sm64_mesh.metalDraw.addLink(Jump(), sm64_model.metalShader, 0)

	for material in mesh.materials:
		if isinstance(material, B2F_Material_Animated):				
			saveTextureAnimationMaterial(bodyParts, 
				material, sm64_mesh, sm64_model, scene)
		else:
			saveMaterialDefinition(bodyParts, material, sm64_mesh, sm64_model, scene)

	# End shader list only at the very end.
	# texture scale reset prevents weird shading on some enemies (ex. bob-ombs, chuckya)
	sm64_mesh.normalDraw.data.extend(
		RDPSync('pipe') +\
		SetGeometryMode(G_LIGHTING) +\
		ClearGeometryMode(G_TEXTURE_GEN) +\
		SetColorCombination(*S_SHADED_SOLID) +\
		TextureScaleFactor('reset') +\
		#RDPSync('full') +\
		EndDL())

	if sm64_model.useMetalShader:
		# Reset shader properties after drawing metal mario
		sm64_mesh.metalDraw.data.extend(
			RDPSync('pipe') +\
			ClearGeometryMode(G_TEXTURE_GEN) +\
			SetColorCombination(*S_SHADED_SOLID) +\
			TextureScaleFactor('reset') +\
			#RDPSync('full') +\
			EndDL())

	for bodyPart in bodyParts:
		if isOverride:
			sm64_model.overrideMeshes[bodyPart] = sm64_mesh
		else:
			sm64_model.meshes[bodyPart] = sm64_mesh
	
	return isAnimMesh

def saveDefinedLighting(sm64_material, lightData, diffuse):
	diffuseLightCount = min(len(lightData) - 1, maxDiffuseLights)

	# no need to load sync for loading lights. only for loading texels.

	sm64_material.shader.addLink(LoadAmbientLight(
		diffuseLightCount), sm64_material.lighting)
	ambientColor = convertRGB(vector3ComponentMultiply(lightData[-1][0], diffuse))
	sm64_material.lighting.data.extend(
		ambientColor + bytearray([0x0]) +\
		ambientColor + bytearray([0x0]))
		# This extra 3rd/4th data is for alignment.

	for i in range(diffuseLightCount):
		# loading lights, getting pointers to textures: E8, E6, 03, FD, F5
		sm64_material.shader.addLink(
			LoadDiffuseLight(i,diffuseLightCount), sm64_material.lighting)
		diffuseColor = convertRGB(vector3ComponentMultiply(lightData[i][0], diffuse))
		diffuseDir = convertNormal(lightData[i][1])
		sm64_material.lighting.data.extend(
			diffuseColor + bytearray([0x0]) +\
			diffuseColor + bytearray([0x0]) +\
			diffuseDir 	 + bytearray([0x0]) +\
			bytearray(4 * [0x0]))  
			# This extra 4th data is only because light data ALSO needs to be 64bit aligned
			# (end in nibble 0x0 or 0x8), so 0x12 length diffuse will break that.

def saveLighting(sm64_material, b2fmaterial, scene):
	lightData = []
	diffuse = mathutils.Vector((1,1,1))
	if b2fmaterial is not None and b2fmaterial.material is not None:	
		if 'sm64_lights' in b2fmaterial.material:
			for lightName in b2fmaterial.material['sm64_lights']:
				light = scene.objects[lightName]
				color = mathutils.Vector(
					(light.data.color.r, light.data.color.g, light.data.color.b))
				direction = (light.matrix_world.to_quaternion() * \
					mathutils.Vector((0,0,1))).normalized()
				lightData.append((color, direction))
		else:
			# default lighting
			lightData.extend(defaultLighting)

		# diffuse color material converted to colored lighting.
		if not isinstance(b2fmaterial, B2F_Material_Animated) and \
			not b2fmaterial.texture0 and not b2fmaterial.texture1 and \
			'sm64_cc' not in b2fmaterial.material:
			diffuse = mathutils.Vector(
				(b2fmaterial.diffuse.r, b2fmaterial.diffuse.g, b2fmaterial.diffuse.b))

		saveDefinedLighting(sm64_material, lightData, diffuse)
	else :
		lightData.extend(defaultLighting)
		saveDefinedLighting(sm64_material, lightData, diffuse)

# not enough cycle space for tinted textures.
def saveMaterialDefinition(bodyParts, material, sm64_mesh, sm64_model, scene):

	# used for vertex coloring
	diffuseColor = material.diffuse[0:3]

	if material not in sm64_model.materials:
		sm64_material = SM64_uMaterial(sm64_model.levelData)
		materialCode, setGeoFlags, clearGeoFlags = getSM64Material(material)

		# setting up shading: E7, B7, FC, BB 
		sm64_material.shader.data.extend(materialCode)
			
		sm64_material.clearGeoFlags = clearGeoFlags

		ambient = bytearray([int(n * 255) for n in material.ambient[0:3]])
		diffuse = bytearray([int(n * 255) for n in material.diffuse[0:3]])
		specular = bytearray([int(n * 255) for n in material.specular[0:3]])
		alpha = int(material.dissolve * 255)

		if material.texture0:
			nextTMEMoffset0, width0, height0 = saveTextureDefinition(
				defaultTileId, sm64_material, sm64_model, material.texture0,
				material.material, 0, diffuse, alpha)
			
			# Ignore diffuse if texture present
			diffuseColor = [1,1,1]

			sm64_material.width = width0
			sm64_material.height = height0

		if material.texture1:
			nextTMEMoffset1, width1, height1 = saveTextureDefinition(
				defaultTileId + 1, sm64_material, sm64_model, material.texture1,
				material.material, nextTMEMoffset0, diffuse, alpha)

			# Ignore diffuse if texture present
			diffuseColor = [1,1,1]

			if width0 != width1 or height0 != height1:
				print("Error: texture0 and texture1 dimensions do not match.")

		# Doesn't matter cause solid shaded doesn't use UVs.
		if not material.texture0 and not material.texture1:
			sm64_material.width = 32
			sm64_material.height = 32

		# Loading light data
		# loading lights, getting pointers to textures: E8, E6, 03, FD, F5
		saveLighting(sm64_material, material, scene)
		sm64_material.shader.data.extend(EndDL())
		sm64_model.materials[material] = sm64_material
	
	sm64_material = sm64_model.materials[material]
	
	# Jump to drawing triangle: 06
	sm64_mesh.normalDraw.addLink(Jump(), sm64_material.shader, 0)

	# See uMesh definition for why this doesn't jump to beginning of draw.
	sm64_mesh.normalDraw.addLink(Jump(), sm64_mesh.draw)
	if sm64_model.useMetalShader:
		sm64_mesh.metalDraw.addLink(Jump(), sm64_mesh.draw)

	# Save vertex and draw data
	saveGeometry(material.vertices, sm64_mesh.vertices, sm64_mesh.draw, 
		sm64_material.width, sm64_material.height, sm64_material.clearGeoFlags & G_LIGHTING,
		diffuseColor)

	return []

def saveTextureAnimationMaterial(bodyParts, material, sm64_texAnimMesh, sm64_model, scene):

	materialCode, setGeoFlags, clearGeoFlags = getSM64Material(material)

	# used for vertex coloring
	# for textures, ignore diffuse color (aka white diffuse)
	diffuseColor = [1,1,1]

	ambient = bytearray([int(n * 255) for n in material.ambient[0:3]])
	diffuse = bytearray([int(n * 255) for n in material.diffuse[0:3]])
	specular = bytearray([int(n * 255) for n in material.specular[0:3]])
	alpha = int(material.dissolve * 255)

	shaderStarts = []
	nextTMEMoffset = 0
	# save texture data
	for i in range(sm64_texAnimMesh.frameCount):
		# Here, materials are not just material definitions but also include jump to draws, and 
		# non return jump to start of normalDraw.
		sm64_material = sm64_texAnimMesh.animMatDraws[i]
		sm64_material.clearGeoFlags = clearGeoFlags

		# setting up shading: E7, B7, FC, BB 
		sm64_material.shader.data.extend(materialCode)

		# loading lights, getting pointers to textures: E8, E6, 03, FD, F5
		saveLighting(sm64_material, material, scene)

		nextTMEMoffset, width, height = saveTextureDefinition(defaultTileId, sm64_material, 
			sm64_model, material.textures[i], material.material, 0, diffuse, alpha)
		sm64_material.shader.addLink(Jump(), sm64_texAnimMesh.draw)
		sm64_material.shader.addLink(Jump(False), sm64_texAnimMesh.normalDraw, 0)
	
	sm64_model.materials[material] = sm64_material
	sm64_material = sm64_model.materials[material]

	# Jump to drawing triangle: 06
	if sm64_model.useMetalShader:
		sm64_texAnimMesh.metalDraw.addLink(Jump(), sm64_texAnimMesh.draw)

	# Save vertex and draw data
	saveGeometry(material.vertices, sm64_texAnimMesh.vertices, sm64_texAnimMesh.draw, 
		width, height, sm64_material.clearGeoFlags & G_LIGHTING, diffuseColor, True)

def saveTextureDefinition(tileId, sm64_material, sm64_model, b2fTexture, blenderMaterial,
	offsetTMEM, diffuse, alpha):
	if b2fTexture is None:
		print('Cannot save texture definition, no texture.')
		return offsetTMEM, 32, 32

	# tile sync first
	sm64_material.shader.data.extend(RDPSync('tile'))

	# save texture data
	if b2fTexture.image.blenderImage not in sm64_model.textures:
		sm64_texture = SM64_uTexture(sm64_model.levelData)
		saveTexture(b2fTexture, sm64_texture.texture)
		sm64_texture.width = b2fTexture.image.width
		sm64_texture.height = b2fTexture.image.height
		sm64_model.textures[b2fTexture.image.blenderImage] = sm64_texture
	sm64_texture = sm64_model.textures[b2fTexture.image.blenderImage]

	sm64_material.shader.addLink(SetTexturePointer(),
		sm64_texture.texture, 0)
	
	# 16 = size of RGBA16, only supported texture type
	# 64 = 64bit word in TMEM
	dataLength = int(sm64_texture.width * sm64_texture.height * 16 / 64)
	sm64_material.shader.data.extend(ClearTileProps(7, offsetTMEM))

	# loading textures: E6, F3
	sm64_material.shader.data.extend(RDPSync('load'))
	sm64_material.shader.data.extend(LoadBlock(7, sm64_texture.width, sm64_texture.height))
	
	# setting tile properties for newly loaded texture
	sm64_material.shader.data.extend(RDPSync('pipe'))
	clampTextures = b2fTexture is not None and b2fTexture.clamp
	sm64_material.shader.data.extend(SetTilePropsGeneral(sm64_texture.width,
		sm64_texture.height, tileId, offsetTMEM, clampTextures))
	sm64_material.shader.data.extend(SetTileSize(sm64_texture.width, sm64_texture.height))

	return (dataLength + offsetTMEM, sm64_texture.width, sm64_texture.height)

# vertexList		= actual vertices list
# vertices  		= byte array to store vertices to
def saveGeometry(vertexList, vertices, draw, texWidth, texHeight, useVertexColors,
	diffuseColor, hasTexture = True):
	# Structure: UVs (2), Normals (3), Positions (3) => 8 floats
	# In pyWaveFront, no vertices are reused in drawing triangles

	# Vertex buffer is max size 32 verts for f3dex.
	# 16 vert maximum to load in vbuf at once (per call)
	# load in 15 verts at a time to draw 5 tris (last index goes unused)
	numVertsInBuffer = 15
	vertexBufferIndex = 0
	draw.addLink(LoadVertices(numVertsInBuffer), vertices)
	for triangleIndex in range(0, len(vertexList), 8*3):
		for i in [triangleIndex + vertexIndex for vertexIndex in range(0, 8*3, 8)]:
			UVcoords 			= vertexList[i:i+2]
			normalOrColorVector	= vertexList[i+2:i+5]
			positionVector 		= vertexList[i+5:i+8]

			# convert float to short and add data
			vertex = bytearray(0)
			vertex.extend(convertPosition(positionVector))
			vertex.extend([0x00, 0x00]) # padding

			if hasTexture:
				vertex.extend(convertUV(UVcoords, texWidth, texHeight))
				#print(UVcoords)
			else: # use UVs with solid color lookup texture
				vertex.extend(bytearray(4 * [0x00]))
			
			if useVertexColors:
				vertex.extend(convertRGB(
					vector3ComponentMultiply(
						mathutils.Vector(normalOrColorVector),
						mathutils.Vector(diffuseColor))))
			else:
				vertex.extend(convertNormal(normalOrColorVector))			
			vertex.append(0xFF)	
			vertices.data.extend(vertex)

		draw.data.extend(DrawTriangle(vertexBufferIndex, 
			vertexBufferIndex + 1, vertexBufferIndex + 2))
		vertexBufferIndex += 3

		if vertexBufferIndex >= numVertsInBuffer:
			# handle case at end where we load less than 15 verts
			remainingVertCount = \
				len(vertexList[triangleIndex : triangleIndex + 8 * 3 * 5]) / 8
			draw.addLink(LoadVertices(int(remainingVertCount)), vertices)
			vertexBufferIndex = 0

	# B8
	draw.data.extend(EndDL())

# texture 			= actual texture
# textureMicrocode 	= byte array to store texture to
def saveTexture(texture, textureMicrocode):
	image = texture.image.image_data
	if image.format != 'RGBA':
		print('Error: image format is not RGBA.')
		return
	imageBytes = image.get_data(image.format, image.pitch)
	for pixelIndex in range(0, len(imageBytes), 4):
		textureMicrocode.data.extend(convert32to16bitRGBA(imageBytes[pixelIndex : pixelIndex + 4]))