import bpy
import mathutils
import copy

from math import pi

from .sm64_level_parser import *
from .sm64_geolayout_macros import *
from .sm64_f3d_parser import *
from .sm64_utility import *
from .sm64_data import *

blender_modes = {'OBJECT', 'BONE'}
marioNoCapDisabled = True

# This geolayout parser is designed to rip armature / model.
# It will only handle transform/mesh related commands.
# For switch cases, only the first option will be chosen.

def convertToArmature(baseObjectList, globalScale = None):
	bpy.ops.object.mode_set(mode = 'OBJECT')
		
	armature = bpy.data.armatures.new('Armature')
	armatureObj = bpy.data.objects.new('ArmatureObj', armature)
	armatureObj.show_x_ray = True
	armature.show_names = True
	
	bpy.context.scene.objects.link(armatureObj)

	# Assign global scale property
	if globalScale is not None:
		armatureObj['sm64_global_scale'] = globalScale

	# Add root bone
	bpy.context.scene.objects.active = armatureObj
	bpy.ops.object.mode_set(mode = 'EDIT')
	armatureEditBones = armatureObj.data.edit_bones

	rootBone = armatureEditBones.new('sm64_bone')
	rootName = rootBone.name

	rootBone.tail = (0,0,0)
	rootBone.head = (0,0, -0.3)
	boneDict = {rootName: baseObjectList}
	
	# MUST start as True.
	for baseObject in baseObjectList:
		# reset root position to make it easier to add bones
		# (i.e. world space now equals pose space)
		baseObject.location = (0,0,0)
		for child in baseObject.children:
			heirarchyToArmature(child, boneDict, armatureObj, rootName)
			# delete unused root bone
			#armatureEditBones.remove(rootBone)

	bindArmatureConstraints(boneDict, armatureObj)

	return armatureObj

# Since recursion is all done in EDIT mode, no need to worry about
# invalid bone references.
def heirarchyToArmature(currentObj, boneDict, armatureObj, parentName,
	useParentTail = False):

	bpy.context.scene.objects.active = armatureObj
	bpy.ops.object.mode_set(mode = 'EDIT')
	armatureEditBones = armatureObj.data.edit_bones
	parentBone = armatureEditBones[parentName]	

	# We reuse the parent tail and extrude the child, or extrude a new parent
	# from the grandparent and then extrude the child.
	if useParentTail:
		parentBone.tail = currentObj.matrix_world.translation
		extrusionBone = parentBone
	else:
		bpy.ops.armature.select_all(action = 'DESELECT')
		armatureEditBones.active = parentBone
		parentBone.select_head = False
		parentBone.select_tail = True
		bpy.ops.armature.extrude()
		extrusionBone = armatureEditBones.active

	bpy.ops.armature.select_all(action = 'DESELECT')
	armatureEditBones.active = extrusionBone
	extrusionBone.select_head = False
	extrusionBone.select_tail = True
	bpy.ops.armature.extrude()

	# body part should pivot around the bone HEAD.
	# bone tail is given arbitrary offset in in local X+ to
	# prevent it from being deleted.
	currentBone = armatureEditBones.active
	currentName = currentBone.name
	currentBone.head = currentObj.matrix_world.translation
	#currentBone.tail = mathutils.Vector(currentBone.head) + \
	#	currentObj.matrix_world.to_quaternion() * \
	#	mathutils.Vector((0.15, 0, 0))
	currentBone.tail = mathutils.Vector(currentBone.head) + \
		mathutils.Vector((0, 0, 0.3))

	# Save constraint mapping for later. This is done since we cannot
	# constraint a mesh until we know where its child pivot is, which
	# will affect rotation.
	if currentName in boneDict:
		boneDict[currentName].append(currentObj)
	else:
		boneDict[currentName] = [currentObj]

	# Add bone translation data so that it can later be used
	# for reimporting. Excludes 0x15 display lists, which have no offset.
	if 'sm64_geo_meta' in currentObj:
		currentBone['sm64_geo_meta'] = currentObj['sm64_geo_meta']
		del currentObj['sm64_geo_meta']

	# Handle zero length bone.
	if (currentBone.head - extrusionBone.head).length < 0.00001:

		handleOverlapGeoMetadata(currentBone)
		if currentObj.parent is not None and useParentTail:
			# remove previous references to object, and add to current bone.	
			for name, objs in boneDict.items():
				if currentObj.parent in objs:
					objs.remove(currentObj.parent)
	
			boneDict[currentName].append(currentObj.parent)

	# Make assumption that for multiple children, the current mesh
	# is constrained to the bone connecting to a child with a mesh.
	meshChildren = [child for child in currentObj.children \
		if ('left' not in child.name and 'right' not in child.name)]
	hasMeshChildren = len(meshChildren) > 0

	parentTailUsed = False
	if hasMeshChildren:
		heirarchyToArmature(meshChildren[0], boneDict, armatureObj, 
				currentName, True)
		parentTailUsed = True
	
	for child in currentObj.children:
		# DONT DO child is meshChildren[0], which will not work
		if hasMeshChildren and child == meshChildren[0]:
			continue
		heirarchyToArmature(child, boneDict, armatureObj, 
			currentName, not parentTailUsed)
		parentTailUsed = True

# we want to bind constraints after completing entire armature,
# since we don't know bone tail positions until that bone's child is processed.
def bindArmatureConstraints(boneDict, armatureObj):
	bpy.ops.object.mode_set(mode = 'OBJECT')

	for bone in armatureObj.pose.bones:
		bone.rotation_mode = 'XYZ'

	for boneName, currentObjs in boneDict.items():
		for currentObj in currentObjs:
			bpy.ops.object.select_all(action = 'DESELECT')
			currentObj.select = True
			bpy.context.scene.objects.active = currentObj
	
			# clear object parent, keep transform	
			bpy.ops.object.parent_clear(type = 'CLEAR_KEEP_TRANSFORM')
	
			if boneName in [bone.name for bone in armatureObj.data.bones]:
				# create 'child of' constraint on object
				constraint = currentObj.constraints.new(type = 'CHILD_OF')
				constraint.target = armatureObj
				constraint.subtarget = boneName
				
				# Set parent_inverse to restore bad transform on constraint application
				# Weird thing where constraint must be attatched to context to use operators
				bpy.context.scene.objects.active = currentObj
				my_context = bpy.context.copy()
				my_context["constraint"] = constraint
				bpy.ops.constraint.childof_set_inverse(my_context, constraint = constraint.name)
				armatureObj.data.bones[boneName].name = currentObj.name
	
			# Set bone connection
			bpy.context.scene.objects.active = armatureObj
			bpy.ops.object.mode_set(mode = 'EDIT')
		
			for currentBone in armatureObj.data.edit_bones:
				currentBone.use_connect = True
		
			bpy.ops.object.mode_set(mode = 'OBJECT')

def parseGeoLayout(romfile, startAddress, level, armatureData,
	convertTransformMatrix, useArmature = False, shadeSmooth = True):
	currentAddress = startAddress
	romfile.seek(currentAddress)
	currentCmd = romfile.read(2)
	objList = []

	if armatureData is not None and \
		armatureData.globalScaleAddrOffset is not None:
		globalScaleOffset = armatureData.globalScaleAddrOffset
		
		romfile.seek(startAddress + globalScaleOffset + 4)
		scale = int.from_bytes(romfile.read(4), 'big') / 0x10000
	else:
		globalScaleOffset = None

	levelParsed = parseLevelAtPointer(romfile, level_pointers[level])
	segmentData = levelParsed.segmentData

	# Pretend that command starts with an 0x04
	currentAddress, objList = parseNode(
		romfile, startAddress, currentAddress - 4, [0x04, 0x00], [currentAddress],
		 mathutils.Matrix.Identity(4),
		segmentData = segmentData, armatureData = armatureData, shadeSmooth = shadeSmooth)
	#print(objList)

	allObjList = []
	for readObj in objList:
		bpy.ops.object.select_all(action = 'DESELECT')
		readObj.select = True
		bpy.context.scene.objects.active = readObj
		if globalScaleOffset is not None:
			readObj['sm64_global_scale'] = scale
		bpy.ops.object.select_grouped(extend = True, type='CHILDREN_RECURSIVE')
		#for obj in bpy.context.selected_objects:
		#	obj['sm64_level'] = level
		allObjList.extend(bpy.context.selected_objects)

	for readObj in objList:
		bpy.ops.object.select_all(action = 'DESELECT')
		readObj.select = True
		bpy.context.scene.objects.active = readObj
		#bpy.ops.object.scale_clear()
		readObj.matrix_world = convertTransformMatrix.to_4x4().inverted()
		#print(readObj.matrix_world)

		# Don't apply on children if you want leaf nodes of armature
		# to point in local space.
		bpy.ops.object.select_grouped(extend = True, type='CHILDREN_RECURSIVE')
		bpy.ops.object.transform_apply(rotation = True)

	if useArmature:
		convertToArmature(objList, scale if globalScaleOffset is not None else None)
	
	return allObjList

# Every node is passed in the address of its OWN command.
# Every node returns the address AFTER the end of its processing extent.
# Make sure to NOT create blender objects if the node is being ignored.
def parseNode(romfile, geoStartAddress, currentAddress, currentCmd, jumps,
	currentTransform = mathutils.Matrix.Identity(4), 
	ignore = False, singleChild = False, endCmd = GEO_NODE_CLOSE,
	segmentData = None, armatureData = None, shadeSmooth = True):
	print("NODE " + hex(currentAddress))
	objList = []
	originalTransform = currentTransform
	currentAddress += getGeoLayoutCmdLength(*currentCmd)
	romfile.seek(currentAddress)
	currentCmd = romfile.read(2)
	lastTransRotAddr = None

	nodeProcessed = False
	switchActive = False
	while currentCmd[0] is not endCmd and currentCmd[0] is not GEO_END:
		ignoreNode = ignore or (singleChild and nodeProcessed)

		if currentCmd[0] == GEO_NODE_OPEN:
			currentAddress, childObjList = parseNode(romfile, 
				geoStartAddress, currentAddress, currentCmd, jumps, 
				currentTransform, ignoreNode, singleChild = switchActive, 
				segmentData = segmentData, armatureData = armatureData, 
				shadeSmooth = shadeSmooth)
			switchActive = False
			currentTransform = originalTransform

		elif currentCmd[0] == GEO_SWITCH:
			currentAddress += getGeoLayoutCmdLength(*currentCmd)
			switchActive = True

		elif currentCmd[0] == GEO_BRANCH:
			currentAddress = parseBranch(
				romfile, currentCmd, currentAddress, jumps, ignoreNode,
				segmentData = segmentData)

		elif currentCmd[0] == GEO_RETURN:
			currentAddress = parseReturn(
				currentCmd, currentAddress, jumps)

		elif currentCmd[0] == GEO_LOAD_DL:
			currentAddress, childObjList = parseDL(romfile, geoStartAddress,
				currentAddress, currentTransform, currentCmd, ignoreNode, lastTransRotAddr, 
				segmentData = segmentData, armatureData = armatureData, 
				shadeSmooth= shadeSmooth)
			lastTransRotAddr = None

		elif currentCmd[0] == GEO_LOAD_DL_W_OFFSET:
			currentAddress, childObjList = parseDLWithOffset(romfile, geoStartAddress,
				currentAddress, currentTransform, currentCmd, jumps, ignoreNode,
				segmentData = segmentData, armatureData = armatureData, 
				shadeSmooth = shadeSmooth)

		elif currentCmd[0] == GEO_TRANSLATE_ROTATE:
			currentAddress, currentTransform, lastTransRotAddr = parseTranslateRotate(romfile, 
				currentAddress, currentCmd, currentTransform, jumps, ignoreNode,
				segmentData = segmentData)

		elif currentCmd[0] == GEO_SCALE:
			currentAddress, currentTransform = parseScale(romfile, 
				currentAddress, currentCmd, currentTransform, jumps, ignoreNode,
				segmentData = segmentData)	
		else:
			currentAddress += getGeoLayoutCmdLength(*currentCmd)
			print("Unhandled command: " + hex(currentCmd[0]))

		if currentCmd[0] in nodeCmds and not ignoreNode:
			nodeProcessed = True
			objList.extend(childObjList)

		romfile.seek(currentAddress)
		currentCmd = romfile.read(2)

	if currentCmd[0] == GEO_END:
		currentAddress = jumps.pop()
	else:
		currentAddress += getGeoLayoutCmdLength(*currentCmd)
	return (currentAddress, objList)

def parseDL(romfile, geoStartAddress, currentAddress, currentTransform, currentCmd, 
	ignoreNode, lastTransRotAddr, segmentData = None, armatureData = None, 
	shadeSmooth = True):

	print("DL " + hex(currentAddress))
	romfile.seek(currentAddress)

	commandStartAddress = currentAddress
	startRelativeOffset = currentAddress - geoStartAddress

	command = romfile.read(getGeoLayoutCmdLength(*currentCmd))
	currentAddress += getGeoLayoutCmdLength(*currentCmd)
	hasMeshData = int.from_bytes(command[4:8], 'big') != 0

	boneName, useNameDict = getMarioBoneName(
		startRelativeOffset, armatureData)

	if ignoreNode:	
		return (currentAddress, None)
	elif not hasMeshData:
		emptyMesh = bpy.data.meshes.new('sm64_empty-mesh')
		obj = bpy.data.objects.new(boneName, emptyMesh)
		bpy.context.scene.objects.link(obj)
		obj.matrix_basis = currentTransform

		assignMarioGeoMetadata(obj, 
			commandStartAddress, geoStartAddress, 
			GEO_LOAD_DL, armatureData, lastTransRotAddr)

		return (currentAddress, [obj])
	else:
		drawLayer = command[1]
		displayListStartAddress = decodeSegmentedAddr(command[4:8], 
			segmentData = segmentData)

		obj = F3DtoBlenderObject(romfile, displayListStartAddress, 
			bpy.context.scene, newname = boneName, 
			segmentData = segmentData, shadeSmooth = shadeSmooth)
		obj.matrix_basis = currentTransform

		assignMarioGeoMetadata(obj, 
			commandStartAddress, geoStartAddress,
			GEO_LOAD_DL, armatureData, lastTransRotAddr)

		return (currentAddress, [obj])

def parseDLWithOffset(romfile, geoStartAddress, currentAddress, 
	currentTransform, currentCmd, jumps, 
	ignoreNode, segmentData = None, armatureData = None, shadeSmooth = True):
	print("DL_OFFSET " + hex(currentAddress))
	romfile.seek(currentAddress)

	# Used for uniquely identifying bones in armature.
	commandStartAddress = currentAddress
	startRelativeOffset = currentAddress - geoStartAddress

	command = romfile.read(getGeoLayoutCmdLength(*currentCmd))

	drawLayer = command[1]

	translationVector = readVectorFromShorts(command, 2)
	translation = mathutils.Matrix.Translation(
		mathutils.Vector(translationVector))

	# Handle parent object and transformation
	# Note: Since we are storing the world transform for each node,
	# we must set the transforms in preorder traversal.
	segmentedAddr = command[8:12]
	hasMeshData = int.from_bytes(segmentedAddr, 'big') != 0
	if not ignoreNode:
		boneName, useNameDict = getMarioBoneName(
			startRelativeOffset, armatureData)

	# Handle child objects
	# Validate that next command is 04 (open node)
	currentAddress += getGeoLayoutCmdLength(*currentCmd)
	romfile.seek(currentAddress) # DONT FORGET THIS, FIXES PARENTING BUG
	currentCmd = romfile.read(2)
	if currentCmd[0] != GEO_NODE_OPEN:
		print("0x13 is not followed by 0x04.")
		childObjList = []
	else:
		# parse following node, resetting accumulated local transform
		currentAddress, childObjList = parseNode(romfile, 
			geoStartAddress, currentAddress,  currentCmd, jumps,
			ignore = ignoreNode,
			segmentData = segmentData, armatureData = armatureData,
			shadeSmooth = shadeSmooth)

	# child objects
	if not ignoreNode:

		# load mesh data
		if hasMeshData:
			displayListStartAddress = decodeSegmentedAddr(segmentedAddr, 
				segmentData = segmentData)
			parentObj = F3DtoBlenderObject(romfile, displayListStartAddress, 
				bpy.context.scene, newname = boneName, 
				segmentData = segmentData, shadeSmooth = shadeSmooth)

			assignMarioGeoMetadata(parentObj, commandStartAddress, 
				geoStartAddress, GEO_LOAD_DL_W_OFFSET, armatureData)
			
		# create empty mesh
		else:
			emptyMesh = bpy.data.meshes.new('sm64_empty-mesh')
			parentObj = bpy.data.objects.new(boneName, emptyMesh)
			bpy.context.scene.objects.link(parentObj)

			assignMarioGeoMetadata(parentObj, commandStartAddress, 
				geoStartAddress, GEO_LOAD_DL_W_OFFSET, armatureData)

		bpy.ops.object.select_all(action = 'DESELECT')
		for childObj in childObjList:
			childObj.select = True
		bpy.context.scene.objects.active = parentObj
		bpy.ops.object.parent_set()
		bpy.ops.object.select_grouped(type='CHILDREN')
		bpy.ops.object.parent_clear(type = 'CLEAR_INVERSE')
		parentObj.matrix_basis = translation * currentTransform

		return (currentAddress, [parentObj])
	else:
		return (currentAddress, childObjList)

def parseBranch(romfile, currentCmd, currentAddress, jumps, ignoreNode,
	segmentData = None):
	print("BRANCH " + hex(currentAddress))
	romfile.seek(currentAddress)
	postJumpAddr = currentAddress + getGeoLayoutCmdLength(*currentCmd)
	currentCmd = romfile.read(getGeoLayoutCmdLength(*currentCmd))
	if not ignoreNode:
		if currentCmd[1] == 1:
			jumps.append(postJumpAddr)
		currentAddress = decodeSegmentedAddr(currentCmd[4:8],
			segmentData = segmentData)
	else:
		currentAddress = postJumpAddr

	return currentAddress

def parseReturn(currentCmd, currentAddress, jumps):
	print("RETURN " + hex(currentAddress))
	currentAddress = jumps.pop()
	return currentAddress

def parseScale(romfile, currentAddress, currentCmd, currentTransform, 
	jumps, ignoreNode, segmentData = None):
	print("SCALE " + hex(currentAddress))
	romfile.seek(currentAddress)
	command = romfile.read(getGeoLayoutCmdLength(*currentCmd))

	scale = int.from_bytes(command[4:8], 'big') / 0x10000

	currentAddress += getGeoLayoutCmdLength(currentCmd[0], currentCmd[1])
	return (currentAddress, mathutils.Matrix.Scale(scale, 4) * currentTransform)
		

def parseTranslateRotate(romfile, currentAddress, 
	currentCmd, currentTransform, jumps, ignoreNode,
	segmentData = None):
	cmdAddress = currentAddress
	print("TRANSLATE_ROTATE " + hex(currentAddress))
	romfile.seek(currentAddress)
	command = romfile.read(getGeoLayoutCmdLength(*currentCmd))

	pos = readVectorFromShorts(command, 4)
	rot = readEulerVectorFromShorts(command, 10, False)

	rotation = mathutils.Euler(rot).to_matrix().to_4x4()
	translation = mathutils.Matrix.Translation(
		mathutils.Vector(pos))

	currentAddress += getGeoLayoutCmdLength(currentCmd[0], currentCmd[1])

	return (currentAddress, rotation * translation * currentTransform, cmdAddress)

def getMarioBoneName(startRelativeAddr, armatureData, default = 'sm64_mesh'):
	try:
		boneName = armatureData.findBoneByOffset(startRelativeAddr).name
		return (boneName, True)
	except Exception:
		return (default, False)

def assignMarioGeoMetadata(obj, commandAddress, geoStartAddress, 
	cmdType, armatureData, lastTransRotAddr = None):

	# for geo_pointer reading offsets:
		# cmd 			= 0
		# draw layer 	= 1
		# translation 	= 2 (for 0x13)
		# display lists = 4 (for 0x15) or 8 (for 0x13)
	sm64_geo_meta = {
		'geo_start' 			: geoStartAddress,
		'geo_pointer_relative' 	: commandAddress - geoStartAddress,
		'geo_cmd_type'			: cmdType,
		'geo_has_mesh'			: len(obj.data.loops) > 0,
		'geo_top_overlap_ptr'	: None,
		'geo_other_overlap_ptrs': [] 
	}

	# actual value doesn't matter, just a flag
	if lastTransRotAddr is not None:
		sm64_geo_meta['geo_trans_ptr'] = lastTransRotAddr
	
	obj['sm64_geo_meta'] = sm64_geo_meta

	if armatureData is not None:
		obj['sm64_part_names'] = [armatureData.findBoneByOffset(
			commandAddress - geoStartAddress).name]

def handleOverlapGeoMetadata(bone):
	parentBone = bone.parent
	if parentBone is not None and 'sm64_geo_meta' in parentBone:
		#boneMeta = copyBlenderPropDict(bone['sm64_geo_meta'])
		boneMeta = bone['sm64_geo_meta']
		parentBoneMeta = parentBone['sm64_geo_meta']

		if parentBoneMeta['geo_top_overlap_ptr'] is None:
			parentBoneMeta['geo_top_overlap_ptr'] = \
				parentBoneMeta['geo_pointer_relative']

		else:
			# Done this way since we cannot deepcopy blender ID property arrays
			parentBoneMeta['geo_other_overlap_ptrs'] = \
				[ptr for ptr in parentBoneMeta['geo_other_overlap_ptrs']] + \
				[parentBoneMeta['geo_pointer_relative']]

		parentBoneMeta['geo_pointer_relative'] = \
			boneMeta['geo_pointer_relative']
		parentBoneMeta['geo_cmd_type'] = \
			boneMeta['geo_cmd_type']
		parentBoneMeta['geo_has_mesh'] = \
			boneMeta['geo_has_mesh']

		bone['sm64_geo_meta'] = parentBoneMeta
		del parentBone['sm64_geo_meta']