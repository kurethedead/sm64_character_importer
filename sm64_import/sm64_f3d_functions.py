from .sm64_f3d_macros import *
from .sm64_f3d_functions_bitwise import *
from .sm64_data import *
import math
# Note that segmented addresses are usually 0 because we will edit it later 
# when we apply relative offets.

# FC
def SetColorCombination(a, b, c, d, e, f, g, h,
						i, j, k, l, m, n, o, p):
	return encodeFC(a, b, c, d, e, f, g, h,
			i, j, k, l, m, n, o, p)

# B7
def SetGeometryMode(flags):
	command = bytearray(4)
	command[0] = G_SETGEOMETRYMODE
	return command + flags.to_bytes(4, 'big')

# B6
def ClearGeometryMode(flags):
	command = bytearray(4)
	command[0] = G_CLEARGEOMETRYMODE
	return command + flags.to_bytes(4, 'big')

# E6-9
def RDPSync(option):
	command = bytearray(8)
	if option == 'load':
		command[0] = G_RDPLOADSYNC	
	elif option == 'pipe':
		command[0] =  G_RDPPIPESYNC	
	elif option == 'tile':
		command[0] =  G_RDPTILESYNC	
	elif option == 'full':
		command[0] =  G_RDPFULLSYNC
	else:
		print("Invalid RDPSync option: " + option)
		return None

	return command

# BB
def TextureScaleFactor(option):
	command = bytearray(4)
	command[0] = G_TEXTURE

	if option == 'standard':
		command[3] = 0x01
		return command + bytes.fromhex('FF FF FF FF')
	elif option == 'environment':
		command[3] = 0x01
		return command + bytes.fromhex('0F 80 07 C0')
	elif option == 'reset':
		command[3] = 0x00
		return command + bytes.fromhex('FF FF FF FF')

# 03
def LoadDiffuseLight(lightIndex, currentDiffuseLightCount):
	if lightIndex >= currentDiffuseLightCount:
		print("Error: light index is greater than current light count.")
	else:
		return MoveMemory(0, lightIndices[lightIndex], G_MV_LIGHT)

def LoadAmbientLight(currentDiffuseLightCount):
	return MoveMemory(0, lightIndices[currentDiffuseLightCount], G_MV_LIGHT)

# DMA commands
def MoveMemory(DRAMSegAddr, DMEMindex, p):
	moveCmd = bytearray(8)
	moveCmd[0] = G_MOVEMEM
	moveCmd[1] = DMEMindex
	moveCmd[2:4] = p.to_bytes(2, 'big')
	moveCmd[4:8] = DRAMSegAddr.to_bytes(4, 'big')
	return moveCmd

# FD
# Assumes RGBA16.
def SetTexturePointer(pointer = bytearray([0, 0, 0, 0])):
	return bytearray.fromhex('FD 10 00 00') + pointer

# Set the starting position of the tile in TMEM
# F5

def num64BitValuesPerRow(width, pixelBitSize):
	return pixelBitSize * width / 64

def SetTileProps32x32(tileID, offsetTMEM, clampTextures):
	return SetTileProperties(32, offsetTMEM, tileID, 5, 5, 
		tClampMirrorFlags = 2 if clampTextures else 0,
		sClampMirrorFlags = 2 if clampTextures else 0)

def SetTileProps64x32(tileID, offsetTMEM, clampTextures):
	return SetTileProperties(64, offsetTMEM, tileID, 5, 6, 
		tClampMirrorFlags = 2 if clampTextures else 0,
		sClampMirrorFlags = 2 if clampTextures else 0)

def SetTilePropsGeneral(width, height, tileID, offsetTMEM, clampTextures):
	masks = int(math.ceil(math.log(width, 2)))
	maskt = int(math.ceil(math.log(height, 2)))
	clampMirrorFlags = 2 if clampTextures else 0
	return SetTileProperties(width, offsetTMEM, tileID, maskt, masks,
		tClampMirrorFlags = clampMirrorFlags, 
		sClampMirrorFlags = clampMirrorFlags)

def ClearTileProps(tileID, offsetTMEM):
	return SetTileProperties(0, offsetTMEM, tileID, 0, 0)

def SetTileProperties(
	textureWidth, offsetTMEM, tileID,  
	tAxisWrapExtent, sAxisWrapExtent,
	colorPallette = 0,
	tClampMirrorFlags = 0, tAxisPerspectiveShift = 0, 
	sClampMirrorFlags = 0, sAxisPerspectiveShift = 0, 	
	colorFormat = colorFormats['RGBA'], 
	pixelBitSizeID = pixelBitSizes['16bit']):

	return encodeF5(colorFormat, pixelBitSizeID, 
		num64BitValuesPerRow(textureWidth, 2 ** (pixelBitSizeID+2)), 
		offsetTMEM, tileID, colorPallette, 
		tClampMirrorFlags, tAxisWrapExtent, tAxisPerspectiveShift, 
		sClampMirrorFlags, sAxisWrapExtent, sAxisPerspectiveShift)

# 06
def Jump(storeReturn = True):
	command = bytearray(8)
	command[0] = G_DL
	if not storeReturn:
		command[1] = 0x01
	return command

# B8
def EndDL():
	command = bytearray(8)
	command[0] = G_ENDDL
	return command

# vertex buffer pos is usually unnecessary, since we load in 
# a new set of vertices for every load
# 04
def LoadVertices(count, vertexBufferPos = 0):
	command = bytearray(8)
	command[0] = G_VTX
	command[1] = (count-1) << 4 | vertexBufferPos
	command[2:4] = (count * 0x10).to_bytes(2, 'big')
	return command

# BF
def DrawTriangle(index1, index2, index3):
	byte1 = index1 * 0xA
	byte2 = index2 * 0xA
	byte3 = index3 * 0xA

	command = bytearray(8)
	command[0] = G_TRI1
	command[5] = byte1
	command[6] = byte2
	command[7] = byte3

	return command

# F2
def SetTileSize(width, height):
	command1 = bytearray(4)
	command1[0] = G_SETTILESIZE
	
	widthValue = (width - 1) << 2
	heightValue = (height - 1) << 2

	# 00 [WW W] [H HH] (i.e. 12 bits per value)
	command2 = widthValue << 12 | heightValue
	command1.extend(command2.to_bytes(4, 'big', signed=True))

	return command1

# dxt = inverse of 64bit words per line (aka width of texture)
# dxt is in fixed point 1.11 format. It should be rounded up beyond 11 bit precision.
# This is why why multiply by 2**11, to convert fixed point to integer representation.
def calculateDXT(width, bitsPerTexel):
	return math.ceil(64 / (width * bitsPerTexel) * 2**11)

#F3
# Since we only use RGBA16, assume 16 bits per texel.
def LoadBlock(tileId, width, height, upperLeftS = 0, upperLeftT = 0, 
	bitsPerTexel = 16):
	command1 = G_LOADBLOCK << 24 | upperLeftS << 12 | upperLeftT
	command2 = tileId << 24 | (width * height - 1) << 12 | calculateDXT(width, bitsPerTexel)

	command1 = command1.to_bytes(4, 'big')
	command2 = command2.to_bytes(4, 'big', signed=True)
	return  command1 + command2

#F4
def LoadTile(tileId, lowerRightS, lowerRightT, 
	upperLeftS = 0, upperLeftT = 0):
	command1 = G_LOADTILE << 24 | upperLeftS << 12 | upperLeftT
	command2 = tileId << 24 | lowerRightS << 12 | lowerRightT

	command1 = command1.to_bytes(4, 'big')
	command2 = command2.to_bytes(4, 'big', signed=True)
	return  command1 + command2

def SetOtherMode(highBits, lowBits):
	command1 = G_RDPSETOTHERMODE << 24 | highBits
	command2 = lowBits

	command1 = command1.to_bytes(4, 'big')
	command2 = command2.to_bytes(4, 'big', signed=True)
	return  command1 + command2

def SetOtherModeL(offset, count, value):
	command = bytearray(8)
	command[0] = G_SETOTHERMODE_L
	command[2] = offset
	command[3] = count
	command[4:8] = value.to_bytes(4, 'big')

	return command

def SetOtherModeH(offset, count, value):
	command = bytearray(8)
	command[0] = G_SETOTHERMODE_H
	command[2] = offset
	command[3] = count
	command[4:8] = value.to_bytes(4, 'big')

	return command

def SetFogColor(hexString):
	data = bytearray.fromhex(hexString)
	command = bytearray(8)
	command[0] = G_SETFOGCOLOR
	command[4:8] = data

	return command

def SetBlendColor(hexString):
	data = bytearray.fromhex(hexString)
	command = bytearray(8)
	command[0] = G_SETFOGCOLOR
	command[4:8] = data

	return command

def SetPrimColor(hexString):
	data = bytearray.fromhex(hexString)
	command = bytearray(8)
	command[0] = G_SETPRIMCOLOR
	command[4:8] = data

	return command

def SetEnvironmentColor(hexString):
	data = bytearray.fromhex(hexString)
	command = bytearray(8)
	command[0] = G_SETENVCOLOR
	command[4:8] = data

	return command