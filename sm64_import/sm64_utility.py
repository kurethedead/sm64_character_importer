from math import pi
from .sm64_data import *
from .sm64_geolayout_macros import *

def printBlenderMessage(msgSet, message, blenderOp):
	if blenderOp is not None:
		blenderOp.report(msgSet, message)
	else:
		print(message)

def bytesToInt(value):
	return int.from_bytes(value, 'big')

def bytesToHex(value, byteSize = 4):
	return format(bytesToInt(value), '#0' + str(byteSize * 2 + 2) + 'x')

def intToHex(value, byteSize = 4):
	return format(value, '#0' + str(byteSize * 2 + 2) + 'x')

def intToBytes(value, byteSize):
	return bytes.fromhex(intToHex(value, byteSize)[2:])

# byte input
# returns an integer, usually used for file seeking positions
def decodeSegmentedAddr(address, segmentData):
	#print(bytesAsHex(address))

	segmentStart = segmentData[address[0]][0]
	return segmentStart + bytesToInt(address[1:4])

#int input
# returns bytes, usually used for writing new segmented addresses
def encodeSegmentedAddr(address, segmentData):
	segment = getSegment(address, segmentData)
	segmentStart = segmentData[segment][0]

	segmentedAddr = address - segmentStart
	return intToBytes(segment, 1) + intToBytes(segmentedAddr, 3)

def getSegment(address, segmentData):
	for segment, interval in segmentData.items():
		if address in range(*interval):
			return segment

	raise ValueError("Address " + hex(address) + \
		" is not found in any of the provided segments.")

# Position
def readVectorFromShorts(command, offset):
	return [readFloatFromShort(command, valueOffset) for valueOffset
		in range(offset, offset + 6, 2)]

def readFloatFromShort(command, offset):
	return int.from_bytes(command[offset: offset + 2], 
		'big', signed = True) * sm64ToBlenderScale

def writeVectorToShorts(command, offset, values):
	for i in range(3):
		valueOffset = offset + i * 2
		writeFloatToShort(command, valueOffset, value[i])

def writeFloatToShort(command, offset, value):
	command[offset : offset + 2] = int(value / sm64ToBlenderScale).to_bytes(
		2, 'big', signed = True)


# Rotation

# Rotation is stored as a short.
# Zero rotation starts at Z+ on an XZ plane and goes counterclockwise.
# 2**16 - 1 is the last value before looping around again.
def readEulerVectorFromShorts(command, offset, signed = False):
	return [readEulerFloatFromShort(command, valueOffset, signed) for valueOffset
		in range(offset, offset + 6, 2)]

def readEulerFloatFromShort(command, offset, signed = False):
	return int.from_bytes(command[offset: offset + 2], 
		'big', signed = signed) / 2**16 * (2 * pi)

def writeEulerVectorToShorts(command, offset, values, signed = False):
	for i in range(3):
		valueOffset = offset + i * 2
		writeEulerFloatToShort(command, valueOffset, values[i], signed)

def writeEulerFloatToShort(command, offset, value, signed = False):
	command[offset : offset + 2] = int(value * 2**16 / (2 * pi)).to_bytes(
		2, 'big', signed = signed)

# convert 32 bit (8888) to 16 bit (5551) color
def convert32to16bitRGBA(oldPixel):
	if oldPixel[3] > 127:
		alpha = 1
	else:
		alpha = 0
	newPixel = 	(oldPixel[0] >> 3) << 11 |\
				(oldPixel[1] >> 3) << 6  |\
				(oldPixel[2] >> 3) << 1  |\
				alpha
	return newPixel.to_bytes(2, 'big')

# convert normalized RGB values to bytes (0-255)
def convertRGB(normalizedRGB):
	return bytearray([
		int(normalizedRGB[0] * 255),
		int(normalizedRGB[1] * 255),
		int(normalizedRGB[2] * 255)
		])
# convert normalized RGB values to bytes (0-255)
def convertRGBA(normalizedRGBA):
	return bytearray([
		int(normalizedRGBA[0] * 255),
		int(normalizedRGBA[1] * 255),
		int(normalizedRGBA[2] * 255),
		int(normalizedRGBA[3] * 255)
		])

def vector3ComponentMultiply(a, b):
	return mathutils.Vector(
		(a.x * b.x, a.y * b.y, a.z * b.z)
	)

# Position values are signed shorts.
def convertPosition(position):
	positionShorts = [int(floatValue / sm64ToBlenderScale) for floatValue in position]
	F3DPosition = bytearray(0)
	for shortData in [shortValue.to_bytes(2, 'big', signed=True) for shortValue in positionShorts]:
		F3DPosition.extend(shortData)
	return F3DPosition

# UVs in F3D are a fixed point short: s10.5 (hence the 2**5)
# fixed point is NOT exponent+mantissa, it is integer+fraction
def convertUV(normalizedUVs, textureWidth, textureHeight):
	#print(str(normalizedUVs[0]) + " - " + str(normalizedUVs[1]))
	F3DUVs = convertFloatToFixed16(normalizedUVs[0] * textureWidth) +\
			 convertFloatToFixed16(normalizedUVs[1] * textureHeight)
	return F3DUVs

def convertFloatToFixed16(value):
	value *= 2**5
	value = min(max(value, -2**15), 2**15 - 1)
	
	return int(value).to_bytes(2, 'big', signed = True)


# Normal values are signed bytes (-128 to 127)
# Normalized magnitude = 127
def convertNormal(normal):
	F3DNormal = bytearray(0)
	for axis in normal:
		F3DNormal.extend(int(axis * 127).to_bytes(1, 'big', signed=True))
	return F3DNormal

def byteMask(data, offset, amount):
	return bitMask(data, offset * 8, amount * 8)
def bitMask(data, offset, amount):
	return (~(-1 << amount) << offset & data) >> offset

def read16bitRGBA(data):
	r = bitMask(data, 11, 5) / ((2**5) - 1)
	g = bitMask(data,  6, 5) / ((2**5) - 1)
	b = bitMask(data,  1, 5) / ((2**5) - 1)
	a = bitMask(data,  0, 1) / ((2**1) - 1)

	return [r,g,b,a]

class MarioArmatureData:
	def __init__(self, bones, scaleAddrOffset = None):

		self.bones = bones
		self.globalScaleAddrOffset = scaleAddrOffset

	def findBoneByName(self, name):
		for bone in self.bones:
			if bone.name == name:
				return bone
		raise ValueError('Bone named ' + name + " not found.")

	def findBoneByOffset(self, offset):
		for bone in self.bones:
			if bone.geo_pointer_relative == offset:
				return bone
		raise ValueError('Bone with offset ' + str(offset) + " not found.")

	def applyTranslationGivenName(self, romfile, geoStartAddress, translation, name, useOverride = False):
		bone = self.findBoneByName(name)
		bone.applyTranslation(romfile, geoStartAddress, translation)
		if useOverride:
			for overrideBone in bone.overrideBoneTranslation:
				self.findBoneByName(overrideBone).applyTranslation(romfile, geoStartAddress, translation)

	def applyDrawLayerGivenName(self, romfile, geoStartAddress, drawLayer, name, useOverride = False):
		bone = self.findBoneByName(name)

		drawLayer = bone.checkOverrideDrawLayer(drawLayer)
			
		bone.applyDrawLayer(romfile, geoStartAddress, drawLayer)
		if useOverride:
			for overrideBone in bone.overrideBoneVisual:
				self.findBoneByName(overrideBone).applyDrawLayer(romfile, geoStartAddress, drawLayer)

	def applyDLPointerGivenName(self, romfile, geoStartAddress, segmentedAddr, metalSegmentedAddr, name,
		useOverride = False):
		bone = self.findBoneByName(name)
		bone.applyDLPointer(romfile, geoStartAddress, 
			segmentedAddr, metalSegmentedAddr)
		
		if useOverride:
			for overrideBone in bone.overrideBoneVisual:
				self.findBoneByName(overrideBone).applyDLPointer(romfile, geoStartAddress, 
					segmentedAddr, metalSegmentedAddr)

	# Aren't these ...GivenOffset functions redundant?
	# Searching by offset to get bone, which applies said offset?
	def applyTranslationGivenOffset(self, romfile, geoStartAddress, translation, offset, useOverride = False):
		bone = self.findBoneByOffset(offset)
		bone.applyTranslation(romfile, geoStartAddress, translation)

		if useOverride:
			for overrideBone in bone.overrideBoneTranslation:
				self.findBoneByName(overrideBone).applyTranslation(romfile, geoStartAddress, translation)

	def applyDrawLayerGivenOffset(self, romfile, geoStartAddress, drawLayer, offset, useOverride = False):
		bone = self.findBoneByOffset(offset)

		drawLayer = bone.checkOverrideDrawLayer(drawLayer)
			
		bone.applyDrawLayer(romfile, geoStartAddress, drawLayer)
		if useOverride:
			for overrideBone in bone.overrideBoneVisual:
				self.findBoneByName(overrideBone).applyDrawLayer(romfile, geoStartAddress, drawLayer)

	def applyDLPointerGivenOffset(self, romfile, geoStartAddress, segmentedAddr, metalSegmentedAddr, offset, 
		useOverride = False):
		bone = self.findBoneByOffset(offset)
		bone.applyDLPointer(romfile, geoStartAddress, 
			segmentedAddr, metalSegmentedAddr)

		if useOverride:
			for overrideBone in bone.overrideBoneVisual:
				self.findBoneByName(overrideBone).applyDLPointer(romfile, geoStartAddress, 
					segmentedAddr, metalSegmentedAddr)


class MarioBoneData:
	def __init__(self, name, geo_pointer_relative, metal_ptrs, 
		other_regular_ptrs, translation_ptr_relative = None, translation_metal_ptr = None, 
		overrideBoneTranslation = [], overrideBoneVisual = [], drawLayerOverride = None):
		self.name = name
		self.geo_pointer_relative = geo_pointer_relative
		self.other_regular_ptrs = other_regular_ptrs
		self.metal_ptrs = metal_ptrs
		self.translation_ptr_relative = translation_ptr_relative
		self.translation_metal_ptr = translation_metal_ptr
		self.overrideBoneTranslation = overrideBoneTranslation
		self.overrideBoneVisual = overrideBoneVisual
		self.drawLayerOverride = drawLayerOverride

	def checkOverrideDrawLayer(self, drawLayer):
		if self.drawLayerOverride is not None:
			return self.drawLayerOverride
		else:
			return drawLayer

	def writeDataAtPointer(self, romfile, option, pointer, data):
		romfile.seek(pointer)
		command = romfile.read(1)

		if command[0] == GEO_LOAD_DL:
			if option == 'translation':
				#print('0x15 command cannot have a translation offset, will skip.')
				return
			elif option == 'draw layer':
				romfile.seek(pointer + 1)
			elif option == 'display list':
				romfile.seek(pointer + 4)
			else:
				raise ValueError("Invalid option " + option + '.')
		elif command[0] == GEO_LOAD_DL_W_OFFSET:
			if option == 'translation':
				romfile.seek(pointer + 2)
			elif option == 'draw layer':
				romfile.seek(pointer + 1)
			elif option == 'display list':
				romfile.seek(pointer + 8)
			else:
				raise ValueError("Invalid option " + option + '.')
		elif command[0] == GEO_TRANSLATE_ROTATE:
			if option == 'translation':
				romfile.seek(pointer + 4)
			else:
				raise ValueError("Invalid option " + option + '.')
		else:
			raise ValueError("Invalid command: " + str(command) +\
				'at ' + hex(pointer) + '.')

		romfile.write(data)

	def applyTranslation(self, romfile, geoStartAddress, translation):
		
		if self.translation_ptr_relative is not None:
			pointer = geoStartAddress + self.translation_ptr_relative
		else:
			pointer = geoStartAddress + self.geo_pointer_relative

		self.writeDataAtPointer(romfile, 'translation', pointer, translation)
		self.writeDataAtPointer(romfile, 'translation', pointer + \
				marioVanishOffsets['regular'], translation)

		for relativePtr in self.other_regular_ptrs:
			pointer = geoStartAddress + relativePtr
			self.writeDataAtPointer(romfile, 'translation', pointer, translation)
			self.writeDataAtPointer(romfile, 'translation', pointer + \
				marioVanishOffsets['regular'], translation)

		if self.translation_ptr_relative is None:
			for relativePtr in self.metal_ptrs:
				pointer = geoStartAddress + relativePtr
				self.writeDataAtPointer(romfile, 'translation', pointer, translation)
				self.writeDataAtPointer(romfile, 'translation', pointer +\
					marioVanishOffsets['metal'], translation)
		else:
			pointer = geoStartAddress + self.translation_metal_ptr
			self.writeDataAtPointer(romfile, 'translation', pointer, translation)
			self.writeDataAtPointer(romfile, 'translation', pointer +\
				marioVanishOffsets['metal'], translation)

	# Vanish always on layer 5
	def applyDrawLayer(self, romfile, geoStartAddress, drawLayer):
		
		pointer = geoStartAddress + self.geo_pointer_relative
		self.writeDataAtPointer(romfile, 'draw layer', pointer, bytearray([drawLayer]))
		self.writeDataAtPointer(romfile, 'draw layer', pointer + \
				marioVanishOffsets['regular'], bytearray([0x05]))

		for relativePtr in self.other_regular_ptrs:
			pointer = geoStartAddress + relativePtr
			self.writeDataAtPointer(romfile, 'draw layer', pointer, bytearray([drawLayer]))
			self.writeDataAtPointer(romfile, 'draw layer', pointer + \
				marioVanishOffsets['regular'], bytearray([0x05]))

		for relativePtr in self.metal_ptrs:
			pointer = geoStartAddress + relativePtr
			self.writeDataAtPointer(romfile, 'draw layer', pointer, bytearray([drawLayer]))
			self.writeDataAtPointer(romfile, 'draw layer', pointer +\
				marioVanishOffsets['metal'], bytearray([0x05]))

	# Vanish always on layer 5
	def applyDLPointer(self, romfile, geoStartAddress, 
		segmentedAddr, metalSegmentedAddr):
		
		pointer = geoStartAddress + self.geo_pointer_relative
		self.writeDataAtPointer(romfile, 'display list', pointer, segmentedAddr)
		self.writeDataAtPointer(romfile, 'display list', pointer + \
				marioVanishOffsets['regular'], segmentedAddr)

		for relativePtr in self.other_regular_ptrs:
			pointer = geoStartAddress + relativePtr
			self.writeDataAtPointer(romfile, 'display list', pointer, segmentedAddr)
			self.writeDataAtPointer(romfile, 'display list', pointer + \
				marioVanishOffsets['regular'], segmentedAddr)

		if metalSegmentedAddr is None:
			return

		for relativePtr in self.metal_ptrs:
			pointer = geoStartAddress + relativePtr
			self.writeDataAtPointer(romfile, 'display list', pointer, metalSegmentedAddr)
			self.writeDataAtPointer(romfile, 'display list', pointer +\
				marioVanishOffsets['metal'], metalSegmentedAddr)

basicMarioArmatureData = MarioArmatureData([
	MarioBoneData('bottom',			-10376, [-4864], []),
	MarioBoneData('torso',			-10332, [-4820], []),
	MarioBoneData('head (cap)', 	-10952, [-5308], 
		[-10944, -10936, -10928, -10920, -10912, -10904, -10896], overrideBoneTranslation = ['head (no cap)'],
		overrideBoneVisual = ['head (no cap)']),
	MarioBoneData('head (no cap)', 	-10872, [-5300], 
		[-10864, -10856, -10848, -10840, -10832, -10824, -10816]),
	# Metal wings are considered "other" pointers, since they are cutout textures
	# that don't work well with matcap.
	MarioBoneData('normal wing 2a', -10760, [-5248], [], -10800, -5288, drawLayerOverride = 0x04),
	MarioBoneData('normal wing 2b', -10704, [-5192], [], -10744, -5232, drawLayerOverride = 0x04),
	MarioBoneData('right upper arm',-10272, [-4760], []),
	MarioBoneData('right forearm',	-10256, [-4744], []),
	MarioBoneData('right hand', 	-10632, [-5120], [], 
		overrideBoneVisual = ['right wrist', 'right hand open']),
	MarioBoneData('left upper arm', -10204, [-4692], []),
	MarioBoneData('left forearm', 	-10188, [-4676], []),
	MarioBoneData('left hand', 		-10512, [-5000], [], 
		overrideBoneVisual = ['left wrist', 'left hand open', 
		'left hand peace sign', 'left hand holding cap']),
	MarioBoneData('right thigh', 	-10128, [-4616], []),
	MarioBoneData('right leg', 		-10112, [-4600], []),
	MarioBoneData('right shoe', 	-10096, [-4584], []),
	MarioBoneData('left thigh', 	-10056, [-4544], []),
	MarioBoneData('left leg', 		-10040, [-4528], []),
	MarioBoneData('left shoe', 		-9988 , [-4476], []),

	MarioBoneData('root', 			-10392, [-4880], []),
	MarioBoneData('neck', 			-10316, [-4804], []),
	MarioBoneData('right clavicle', -10288, [-4776], []),
	MarioBoneData('right wrist', 	-10668, 
		[-5156, -5092, -5080, -5068], 
		[-10604, -10592, -10580]),
	MarioBoneData('right hand open', -10616, [-5104], []),
	MarioBoneData('left clavicle', 	-10220, [-4708], []),
	MarioBoneData('left wrist', 	-10548, [-5036], []),
	MarioBoneData('left hand open', -10484, [-4972], []),
	MarioBoneData('left hand peace sign', -10452, [-4940], []),
	MarioBoneData('left hand holding cap', -10440, [-4928, -4916], [-10428]),
	MarioBoneData('right hip',	 	-10144, [-4632], []),
	MarioBoneData('left hip',	 	-10072, [-4560], []),
	MarioBoneData('left ankle', 	-10024, [-4512], []),
	MarioBoneData('cap hand wing',  -10412, [-4900], [])
	], 

	0xC 
)

