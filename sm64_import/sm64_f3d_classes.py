import pprint
import copy
import math
from .sm64_level_parser import *
from .sm64_f3d_functions import *
from .sm64_f3d_parser import *
from .sm64_utility import *
from .sm64_data import *

class SM64_Light:
	def __init__(self, typeVal, color, direction = None):
		self.type = typeVal # {AMBIENT, DIFFUSE}
		self.color = color # RGBA, normalized floats
		self.direction = direction # only used for DIFFUSE

	def to_microcode(self):
		command = bytearray(8 if self.type == 'AMBIENT' else 12)
		command[0:3] = [int(color * 255) for color in self.color]
		command[3] = 0xFF
		command[4:7] = command[0:3]
		command[7] = 0xFF
		
		if self.type == 'DIFFUSE':
			command[8:11] = [int(axis * 255) for axis in self.direction]
			command[11] = 0xFF

		return command

class SM64_Material:
	def __init__(self, color1, alpha1, color2, alpha2, 
		ambientLight, diffuseLights):
		self.ambientLight = ambientLight

		numDiffuseLights = len(diffuseLights)
		if numDiffuseLights > maxDiffuseLights:
			raise ValueError("Too many lights. " +\
				"(max is " + str(maxDiffuseLights) + ", but provided " + \
				str(numDiffuseLights) + ")")
		else:
			self.diffuseLights = diffuseLights

		self.cycle1 = SM64_CC_Cycle(color1, alpha1)
		if color2 is None or alpha2 is None:
			self.cycle2 = copy.deepcopy(self.cycle1)
		else:
			self.cycle2 = SM64_CC_Cycle(color2, alpha2)

		# shade = shading from light data

		# general purpose CC registers:
		self.color_env = None # note: used by SM64 for alpha transparency.
		self.color_prim = None

		# general purpose BL register
		self.color_blend = None
		self.color_fog = None

		# chroma key registers
		self.keyR = SM64_Color_Key(0, 0, 0)
		self.keyG = SM64_Color_Key(0, 0, 0)
		self.keyB = SM64_Color_Key(0, 0, 0)

class SM64_Color_Key:
	def __init__(self, center, scale, width):
		self.center = center
		self.scale = scale
		self.width = width

class SM64_CC_Cycle:
	def __init__(self, color, alpha):
		self.color = SM64_CC_Function('COLOR', *color)
		self.alpha = SM64_CC_Function('ALPHA', *alpha)

class SM64_CC_Function:
	def __init__(self, typeVal, a, b, c, d):
		self.type = typeVal

		if typeVal == 'COLOR':
			self.a = colorSources[a]
			self.b = colorSources[b]
			self.c = colorSources[c] 
			self.d = colorSources[d]
		elif typeVal == 'ALPHA':
			self.a = alphaSources[a]
			self.b = alphaSources[b]
			self.c = alphaSources[c] 
			self.d = alphaSources[d]
		else:
			raise ValueError("Unhandled CC function type: " + typeVal)

class SM64_uMesh:
	def __init__(self, levelData, useMetalShader):
		self.normalDraw = SM64_Microcode(levelData)
		if useMetalShader:
			self.metalDraw	= SM64_Microcode(levelData)

		# Note: draw is composed into multiple submeshes ending in B8.
		# normalDraw will jump to a specific material and then to a specific submesh.
		self.draw  		= SM64_Microcode(levelData)
		self.vertices	= SM64_Microcode(levelData)

class SM64_uTexAnimMesh(SM64_uMesh):
	def __init__(self, levelData, frameCount, useMetalShader):
		super().__init__(levelData, useMetalShader)
		# animShaderDraws consists of a material, texture definition, lighting, 
		# draw jump, then non-return normalDraw jump.
		self.animMatDraws = []
		self.frameCount = frameCount
		for i in range(0, frameCount):
			self.animMatDraws.append(SM64_uMaterial(levelData))

# Note that a shader includes a texture definition in it.
class SM64_uMaterial:
	def __init__(self, levelData):
		self.shader 	= SM64_Microcode(levelData)
		self.lighting 	= SM64_Microcode(levelData)
		self.clearGeoFlags = 0

class SM64_uTexture:
	def __init__(self, levelData):
		self.texture  	= SM64_Microcode(levelData)
		self.width		= 32
		self.height		= 32

class SM64_uModel:
	def __init__(self, levelData, useMetalShader):
		# {blender material/texture/data : uMaterial/Texture/etc.}
		self.defaultDiffuseTextures = {}
		self.textures = {}
		self.materials = {}
		# {bodyPartName : mesh}
		self.meshes = {}
		# this is only so that override mesh geolayout pointers are handled after normal meshes.
		self.overrideMeshes = {}
		self.useMetalShader = useMetalShader
		if useMetalShader:
			self.metalShader = generateMetalShader(levelData)
		self.levelData = levelData

	def getMicrocodeList(self, printTextureList):
		if printTextureList:
			for (bTexture, uTexture) in self.textures.items():
				print(str(bTexture) + " " + hex(uTexture.texture.startAddress))
		uList = []
		if self.useMetalShader:
			uList.extend([self.metalShader])
		uList.extend([uTexture.texture for (texValue, uTexture) in self.defaultDiffuseTextures.items()])
		uList.extend([uTexture.texture for (bTexture, uTexture) in self.textures.items()])
		uList.extend([uMaterial.lighting for (bMaterial, uMaterial) in self.materials.items()])
		uList.extend([uMaterial.shader for (bMaterial, uMaterial) in self.materials.items()])
		for (bodyPart, uMesh) in self.meshes.items():
			if isinstance(uMesh, SM64_uTexAnimMesh):
				uList.extend([uMaterial.shader for uMaterial in uMesh.animMatDraws])
				uList.extend([uMaterial.lighting for uMaterial in uMesh.animMatDraws])
			uList.append(uMesh.normalDraw)
			if self.useMetalShader:
				uList.append(uMesh.metalDraw)
			uList.append(uMesh.draw)
			uList.append(uMesh.vertices)

		for (bodyPart, uMesh) in self.overrideMeshes.items():
			if isinstance(uMesh, SM64_uTexAnimMesh):
				uList.extend([uMaterial.shader for uMaterial in uMesh.animMatDraws])
				uList.extend([uMaterial.lighting for uMaterial in uMesh.animMatDraws])
			uList.append(uMesh.normalDraw)
			if self.useMetalShader:
				uList.append(uMesh.metalDraw)
			uList.append(uMesh.draw)
			uList.append(uMesh.vertices)
		
		return uList

	def setContiguous(self, startAddr):
		currentAddr = startAddr
		for uList in self.getMicrocodeList(False):
			# Handle alignment: f3d must be 0x0 or 0x8 aligned (for console)
			endNibble = hex(currentAddr)[-1]
			if endNibble != '0' and endNibble != '8':
				currentAddr = math.ceil(currentAddr / 8) * 8
			uList.startAddress = currentAddr
			currentAddr += len(uList.data)

		return currentAddr

	def applyRelativeAddresses(self):
		for uList in self.getMicrocodeList(False):
			uList.applyRelativeAddresses()
	
	def writeToFile(self, romfile):
		for uList in self.getMicrocodeList(True):
			uList.writeToFile(romfile)

	def printBreakdown(self):
		for uList in self.getMicrocodeList(False):
			print(str(uList) + ": " + \
				format(len(uList.data), '#08x'))

	def debug(self):
		for uList in self.getMicrocodeList(False):
			uList.debug()

class SM64_Microcode:
	def __init__(self, levelData):
		self.startAddress = 0
		self.data = bytearray(0)
		self.relativeAddresses = {
			# address : (microcode, addressInMicrocode)
			# address = first byte of a segmented address in this microcode
			# addressInMicrocode = address referenced to by this microcode
		}
		self.levelData = levelData

	# once done writing data, you can set the startAddress and then apply the correct
	# offset to any other list pointing to this one.
	def applyRelativeAddresses(self):
		for relativeAddress, destination in self.relativeAddresses.items():
			absoluteAddress = destination[0].startAddress + destination[1]
			segmentedAddress = encodeSegmentedAddr(absoluteAddress, self.levelData)
			self.data[relativeAddress : relativeAddress + 4] = segmentedAddress

	# addLink: extend data that contains a segmented address referring to 
	# a different SM64_Microcode object.

	# byteData = entire command
	# segmentedAddressIndex = byte index in command where segmented address starts
	# microcode = referenced microcode
	# relativeAddress = address in referenced microcode relative to its start address
	def addLink(self, byteData, microcode, relativeAddress = None):
		if relativeAddress is None:
			relativeAddress = len(microcode.data)

		# all F3D commands are 8 bytes, with segmented addresses 4 bytes in
		self.relativeAddresses[len(self.data) + 4] = \
			(microcode, relativeAddress)

		self.data.extend(byteData)

	def writeToFile(self, file):
		file.seek(self.startAddress)
		file.write(self.data)

	def currentEnd(self):
		return self.startAddress + len(self.data)

	def debug(self):
		print(str(self.startAddress) + " - " + str(self.currentEnd()))

def generateMetalShader(levelData):
	metalShader = SM64_Microcode(levelData)
	metalShader.data.extend(
		RDPSync('pipe') +\
		SetGeometryMode(G_TEXTURE_GEN) +\
		SetColorCombination(*S_UNLIT_TEX) +\
		SetTexturePointer(bytearray.fromhex('04 00 00 90')) +\
		SetTileProperties(0, 0, 0x07, 5, 6) +\
		RDPSync('load') +\
		LoadBlock(0x07, 64, 32) +\
		RDPSync('pipe') +\
		SetTilePropsGeneral(64, 32, 0, 0, False) +\
		SetTileSize(64, 32) +\
		TextureScaleFactor('environment') +\
		EndDL()
	)
	return metalShader

def updateGenericGeolayoutPtrsAndLayers(uModel, geoPointerAddresses, drawLayer, romfile):
	for geoPointerAddress in geoPointerAddresses:
		# Assuming only 1 mesh
		uMesh = uModel.meshes[list(uModel.meshes.keys())[0]]
		startAddr = uMesh.normalDraw.startAddress
		romfile.seek(geoPointerAddress + 1)
		romfile.write(bytearray([drawLayer]))

		romfile.seek(geoPointerAddress)
		command = romfile.read(1)
		if command[0] == GEO_LOAD_DL:
			romfile.seek(geoPointerAddress + 4)
			romfile.write(encodeSegmentedAddr(startAddr, uModel.levelData))
		elif command[0] == GEO_LOAD_DL_W_OFFSET:
			romfile.seek(geoPointerAddress + 8)
			romfile.write(encodeSegmentedAddr(startAddr, uModel.levelData))
		else:
			raise ValueError("Command " + hex(command[0]) + ' at ' +\
				hex(geoPointerAddress) + ' is an invalid DL loading command.')

def handleMarioPointer(bodyPart, sm64_mesh, uModel, geoStartAddress, drawLayers, romfileDest, useFaceAnimation):
	encodedStart = encodeSegmentedAddr(
		sm64_mesh.normalDraw.startAddress, uModel.levelData)
	encodedMetalStart = encodeSegmentedAddr(
		sm64_mesh.metalDraw.startAddress, uModel.levelData)

	if bodyPart in drawLayers:
		drawLayer = drawLayers[bodyPart]
	else:
		drawLayer = 0x01
	basicMarioArmatureData.applyDrawLayerGivenName(
		romfileDest, geoStartAddress, drawLayer, bodyPart, True)

	basicMarioArmatureData.applyDLPointerGivenName(
		romfileDest, geoStartAddress, encodedStart, 
		encodedMetalStart, bodyPart, True)

	if useFaceAnimation and isinstance(sm64_mesh, SM64_uTexAnimMesh):
	# Because we process cap head before any override parts (e.g. capless head),
	# its okay that cap head overwrites capless head.
	# capless head, if present, will always come after cap head and overwrite it instead.
		if not (bodyPart == 'head (no cap)' or bodyPart == 'head (cap)'):
			print("Texture animated mesh only supported for head.")
			return

		faceExpressionPtrs = []
		for animMatDraw in sm64_mesh.animMatDraws:
			faceExpressionPtrs.append(animMatDraw.shader.startAddress)
		if bodyPart == 'head (no cap)':
			faceSwitchOffsets = marioFaceSwitchOffsets[1:2]
		else:
			faceSwitchOffsets = marioFaceSwitchOffsets[0:2]

		for vanishOffset in [0, marioVanishOffsets['regular']]:
			for faceOffset in faceSwitchOffsets:
				for i in range(marioFaceExpressionCount):
					offset = i if i < 3 else 7 
					writeAddr = geoStartAddress + faceOffset + vanishOffset + 12 + 8*offset + 4
					romfileDest.seek(writeAddr)
					romfileDest.write(encodeSegmentedAddr(faceExpressionPtrs[i], uModel.levelData))
		for vanishOffset in [0, marioVanishOffsets['metal']]:
			for metalPtr in basicMarioArmatureData.findBoneByName(bodyPart).metal_ptrs:
				writeAddr = geoStartAddress + metalPtr + vanishOffset + 4
				romfileDest.seek(writeAddr)
				romfileDest.write(encodeSegmentedAddr(sm64_mesh.metalDraw.startAddress, uModel.levelData))

def updateMarioGeolayoutPtrsAndLayers(uModel, geoStartAddress, 
	drawLayers, romfileDest, useFaceAnimation):
	for (bodyPart, sm64_mesh) in uModel.meshes.items():
		handleMarioPointer(bodyPart, sm64_mesh, uModel, 
			geoStartAddress, drawLayers, romfileDest, useFaceAnimation)
	for (bodyPart, sm64_mesh) in uModel.overrideMeshes.items():
		handleMarioPointer(bodyPart, sm64_mesh, uModel, 
			geoStartAddress, drawLayers, romfileDest, useFaceAnimation)

class B2F_Obj:
	def __init__(self, partObj, scene, marioForwardAxis, recursive, convertTransformMatrix, animated):
		self.mesh_list = []

		# Select object and children and duplicate
		bpy.ops.object.select_all(action = 'DESELECT')
		partObj.select = True
		scene.objects.active = partObj
		if recursive:
			bpy.ops.object.select_grouped(extend = True, 
						type = 'CHILDREN_RECURSIVE')
		scene.objects.active = partObj
		bpy.ops.object.duplicate()
		dupObj = scene.objects.active

		# Apply modifiers on duplicates
		allObjs = bpy.context.selected_objects
		bpy.ops.object.select_all(action = 'DESELECT')
		for obj in allObjs:
			scene.objects.active = obj
			for modifier in obj.modifiers:
				bpy.ops.object.modifier_apply(modifier = modifier.name)
			scene.objects.active = None

		# Reselect all objects and set root as active object
		dupObj.select = True
		scene.objects.active = dupObj
		bpy.ops.object.select_grouped(extend = True, 
						type = 'CHILDREN_RECURSIVE')
		scene.objects.active = dupObj

		# Join all objects at the root object
		if recursive:
			bpy.ops.object.join()
		dupObj = scene.objects.active

		# clear transform
		bpy.ops.object.rotation_clear()
		bpy.ops.object.location_clear()

		# Apply blender to sm64 coordinate transform
		dupObj.rotation_mode = 'QUATERNION'
		dupObj.rotation_quaternion = getExportRotation(marioForwardAxis, convertTransformMatrix) \
			* dupObj.rotation_quaternion
		bpy.ops.object.transform_apply(rotation=True, scale=True)

		# get mesh (for materials) and triangulated bMesh (for geometry)
		mesh = dupObj.to_mesh(scene, True, 'PREVIEW')
		bMesh = bmesh.new()
		bMesh.from_mesh(mesh)
		bmesh.ops.triangulate(bMesh, faces = bMesh.faces[:], 
			quad_method = 0, ngon_method = 0)

		self.mesh_list.append(B2F_Mesh(mesh, bMesh, animated))

		# clean up data
		bpy.data.objects.remove(dupObj)
		bpy.data.meshes.remove(mesh)
		bMesh.free()

class B2F_Mesh:
	def __init__(self, mesh, bMesh, animated):
		if len(mesh.materials) == 0:
			self.materials = [B2F_Material(None, mesh, bMesh)]
			return

		self.materials = []
		if animated:
			animatedMaterial = None
			for material in mesh.materials:
				if 'sm64_anim_mat' in material:
					animatedMaterial = material
					break
			if animatedMaterial is None:
				print("No material in mesh is marked with 'sm64_anim_mat, importing static texture instead.")
				for material in mesh.materials:
					self.materials.append(B2F_Material(material, mesh, bMesh))
			else:
				self.materials.append(B2F_Material_Animated(animatedMaterial, mesh, bMesh))
				for material in mesh.materials:
					if material != animatedMaterial:
						self.materials.append(B2F_Material(material, mesh, bMesh))
		else:
			for material in mesh.materials:
				self.materials.append(B2F_Material(material, mesh, bMesh))

class B2F_Material:
	def __init__(self, material, mesh, bMesh):
		# single float indicating influence of global ambient color
		self.material = material

		if material is not None:
			self.ambient = [material.ambient * value for value in [0, 0, 0]] 
			self.diffuse = material.diffuse_color
			self.specular = material.specular_color
			self.dissolve = material.alpha
			texSlot0 = material.texture_slots[0]
			texSlot1 = material.texture_slots[1]
			self.texture0 = B2F_Texture(texSlot0.texture) \
				if texSlot0 is not None else None
			self.texture1 = B2F_Texture(texSlot1.texture) \
				if texSlot1 is not None else None

			# triangles = list(tuple(loop))
			triangles = [face.loops for face in bMesh.faces if 
				mesh.materials[face.material_index] == material]
		else:
			self.ambient  = [0,0,0]
			self.diffuse = [1,1,1]
			self.specular = [1,1,1]
			self.dissolve = 1
			self.texture0 = None
			self.texture1 = None

			triangles = [face.loops for face in bMesh.faces]

		self.vertices = []
		uv_layer = bMesh.loops.layers.uv.active
		color_layer = bMesh.loops.layers.color.active
		
		self.setGeoFlags = G_NONE
		self.clearGeoFlags = G_NONE

		if material is not None:
			if 'sm64_setgeomode' in material:
				self.setGeoFlags |= material['sm64_setgeomode']
			if 'sm64_cleargeomode' in material:
				self.clearGeoFlags |= material['sm64_cleargeomode']
			if 'sm64_vertex_color' in material:
				self.clearGeoFlags |= G_LIGHTING
			else:
				self.setGeoFlags |= G_LIGHTING
			
		useVertexColors = self.clearGeoFlags & G_LIGHTING

		# get interleaved data (UV, Normal, Position)
		for triangle in triangles: # loopSequence in list(loopSequence)
			for loop in triangle:	# loop in loopSequence
				uvVector = loop[uv_layer].uv if uv_layer is not None else (0,0)
				if useVertexColors:
					normalOrColorVector = loop[color_layer] if \
						color_layer is not None else (1,1,1)
				else:
					normalOrColorVector = loop.vert.normal.normalized()
				for vectorVal in (uvVector, normalOrColorVector, loop.vert.co): 
					for floatVal in vectorVal:
						self.vertices.append(floatVal)

class B2F_Material_Animated(B2F_Material):
	def __init__(self, material, mesh, bMesh):
		super().__init__(material, mesh, bMesh)
		del self.texture0
		del self.texture1
		self.textures = []
		for i in range(8):
			texSlot = material.texture_slots[i]
			self.textures.append(B2F_Texture(texSlot.texture) \
				if texSlot is not None else None)

		# triangles = list(tuple(loop))
		triangles = [face.loops for face in bMesh.faces if 
			mesh.materials[face.material_index] == material]

		self.vertices = []
		uv_layer = bMesh.loops.layers.uv.active
		color_layer = bMesh.loops.layers.color.active
		useVertexColors = 'sm64_cleargeomode' in material and \
			material['sm64_cleargeomode'] & G_LIGHTING

		# get interleaved data (UV, Normal, Position)
		for triangle in triangles: # loopSequence in list(loopSequence)
			for loop in triangle:	# loop in loopSequence
				uvVector = loop[uv_layer].uv if uv_layer is not None else (0,0)
				if useVertexColors:
					normalOrColorVector = loop[color_layer] if \
						color_layer is not None else (1,1,1)
				else:
					normalOrColorVector = loop.vert.normal
				for vectorVal in (uvVector, normalOrColorVector, loop.vert.co): 
					for floatVal in vectorVal:
						self.vertices.append(floatVal)

class B2F_Texture:
	def __init__(self, texture):
		self.image = B2F_Image(texture.image)
		self.clamp = \
			texture.extension == 'EXTEND' or \
			texture.extension == 'CLIP' or \
			texture.extension == 'CLIP_CUBE'

class B2F_Image:
	def __init__(self, image):
		self.blenderImage = image
		self.image_data = B2F_ImageData(image)
		self.width = image.size[0]
		self.height = image.size[1]

class B2F_ImageData:
	def __init__(self, image):
		self.width = image.size[0]
		self.height = image.size[1]
		self.format = 'RGBA'
		self.depth = 2 ** (32 / len(self.format))
		self.data = [int(value * (self.depth - 1)) for value in image.pixels]

		# number of bytes per row (don't use this)
		self.pitch = 4 * self.width

	def get_data(self, format, pitch):
		return self.data